mtype = { 
  OPEN,         /* Open (doors)          */
  CLOSE,        /* Close (doors)         */
  FLOOR_REQ,    /* Next floor request    */
  FLOOR_REPLY   /* Next floor reply      */
};

#define FLOOR_N	5

#define INVALID 255
#define BASE_FLOOR 2

/*----------------------------------------------------------------------------*/
/* buses */

show chan doors_bus = [0] of { mtype };         /* OPEN, CLOSE; wait till hadnle */
show chan router_bus = [0] of { mtype, byte };  /* FLOOR_REQ, FLOOR_REPLY        */
show chan button_bus = [0] of { byte };

/* state vars */
show byte elevator_pos = BASE_FLOOR;
show bool doors_are_opened = false;
show bool movement_in_progress = false;

show byte delivery_promise = INVALID;
show bool user_waits_elev = false;
show bool user_detected = false;

show bool floor_movement; /* have single user so dont care about floor num*/

/*----------------------------------------------------------------------------*/
/* Elevator (engine) controller */
active proctype elev_ctl() {
    bool direction;
    byte requested_floor;

end:do
    :: true ->
        movement_in_progress = false;
        doors_bus ! OPEN;
        /* stay timer is done -> close the doors */
        doors_bus ! CLOSE;
        (!doors_are_opened); /* wait till doors are closed */

	/* doors are closed and we are ready for ride */
	router_bus ! FLOOR_REQ(elevator_pos);
	router_bus ? FLOOR_REPLY(requested_floor);
        assert(requested_floor >= 0 && requested_floor < FLOOR_N);
        direction = requested_floor > elevator_pos;

        do
        :: requested_floor == elevator_pos ->
      	    movement_in_progress = false;
            doors_bus ! OPEN;
       	    (doors_are_opened);
            break; /* go to control */
        :: else ->
      	    movement_in_progress = true;
	    elevator_pos = elevator_pos + (direction -> 1 : -1);
        od;
    od;
}

/*----------------------------------------------------------------------------*/
/* Doors controller */
active proctype doors_ctl() {
end:do
    :: doors_bus ? CLOSE ->      
        atomic{ !(floor_movement) -> doors_are_opened = false }
    :: doors_bus ? OPEN -> doors_are_opened = true; 
    od;
}

/*----------------------------------------------------------------------------*/
/* Route controller */

active proctype route_ctl() {
    byte next_floor = INVALID;
    byte floor; /* tmp var */  
end: do
    :: button_bus ? floor ->
        next_floor = floor;
    :: router_bus ? FLOOR_REQ(_) ->
       	if
      	:: (next_floor != INVALID) -> 
            router_bus ! FLOOR_REPLY(next_floor);
            next_floor = INVALID;
        :: else ->
      	    router_bus ! FLOOR_REPLY(BASE_FLOOR);
        fi;
    od;
}

/*----------------------------------------------------------------------------*/
/* Man controller */

#define PICK_RANDOM_FLOOR \
  if \
  ::true -> rnd = 0;\
  ::true -> rnd = 1;\
  ::true -> rnd = 2;\
  ::true -> rnd = 3;\
  ::true -> rnd = 4;\
  fi;

#define ELEVATOR_IS_INTERACTABLE(floor)\
  (elevator_pos == floor && doors_are_opened)

#define RIDE_TO_DST_FLOOR\
  printf("MSC: Man enters at floor %d\n", src_floor);\
  floor_movement = false;\
  \
  printf("MSC: Man pushes %d inside\n", dst_floor);\
  button_bus ! dst_floor;\
  atomic {\
    /* wait for ride end */\
    ELEVATOR_IS_INTERACTABLE(dst_floor) -> floor_movement = true;\
  }\
  printf("SUCCESS: User exits on DST the floor %d\n", dst_floor);\
  floor_movement = false;

active proctype man() {
  byte rnd = 0;
  byte src_floor = 0;
  byte dst_floor = 0;

end:
  do
  :: true ->
      user_detected = false;
      PICK_RANDOM_FLOOR
      src_floor = rnd;
      PICK_RANDOM_FLOOR
      dst_floor = rnd;

      if
      :: src_floor == dst_floor -> dst_floor = (dst_floor + 1) % FLOOR_N
      :: else -> skip;
      fi;

      user_detected = true;
      printf("MSC: Man appears on SRC floor %d want to go to %d\n", src_floor, dst_floor);
      
      if
      :: atomic{ ELEVATOR_IS_INTERACTABLE(src_floor) -> floor_movement = true; }
	  RIDE_TO_DST_FLOOR
      :: else ->
          button_bus ! src_floor; /* call the elevator */
      	  if
	  :: true ->
            user_waits_elev = true;
	    
            atomic{ ELEVATOR_IS_INTERACTABLE(src_floor) -> floor_movement = true }
            RIDE_TO_DST_FLOOR
	    user_waits_elev = false;
          ::true ->
            printf("MSC: %d leaves SRC floor  %d\n", _pid, src_floor);
          fi;
      fi;
  od;
}

/*----------------------------------------------------------------------------*/
/* LTL stuff */

/* 1. Ride with closed doors */
ltl p1 {always (movement_in_progress -> (!doors_are_opened)) }

/* 2. Delivery promise always holds */
ltl p2 {
  (always
    ((delivery_promise != INVALID) implies
    (eventually
      (elevator_pos == delivery_promise && doors_are_opened)
    ))
  )
}

/* 3. If movement detected -> doors must be opened */
ltl p3 { always (floor_movement implies doors_are_opened) }

/* 4. Either elevator will be moving or Stay at base floor or No users */
/* BASE_FLOOR must be added since router peek is not determined */
ltl p4 { 
  (([] <> movement_in_progress) || (<> [] (elevator_pos == BASE_FLOOR)) || (<> [] (!user_waits_elev))) 
}

/* 5. If no users -> go to base floor */
ltl p5 {
  [](([] (!user_detected)) ->  (<> [] (elevator_pos == BASE_FLOOR)))
}