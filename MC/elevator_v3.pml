/* Simple elevator model with single user                                     */
/* by Artur Huletski (hatless.fox@gmail.com)                                  */

mtype = {
  OPEN,         /* Open (doors)          */
  CLOSE,        /* Close (doors)         */
};

#define FLOOR_N 5

#define INVALID 255
#define BASE_FLOOR 2

/*----------------------------------------------------------------------------*/
/* Communication stuff */

show chan doors_bus = [0] of { mtype };      /* OPEN, CLOSE; wait till hadnle */
show byte pending_call_request = INVALID;

/* exposed state vars */
show byte elevator_pos = BASE_FLOOR;
show bool elevator_is_moving = false;
show bool doors_are_opened = false;

show byte user_src_floor = INVALID;
show byte user_dst_floor = INVALID;

show byte requested_floor = BASE_FLOOR;

/* system predicates for LTL */
show bool user_is_inside = false;
show bool user_is_waiting_elev = false;
show bool user_detected = false;

show byte user_ordered_floor = INVALID;


show bool floor_movement; /* have single user so dont care about floor num */


/*----------------------------------------------------------------------------*/
/* Elevator controller */
active proctype elev_ctl() {
    bool direction;

end:do
    :: true ->
        elevator_is_moving = false;
        doors_bus ! OPEN;

        /*@@ 'user wait' timer is done -> close the doors */
        doors_bus ! CLOSE; (!doors_are_opened); /* wait till doors are closed */

        /*@@ doors are closed and we are ready for ride */
        if
        :: (pending_call_request != INVALID) || user_is_inside ->
           atomic {
             (pending_call_request != INVALID) ->
               requested_floor = pending_call_request;
             pending_call_request = INVALID;
           }
        :: else ->
           if
           :: elevator_pos == BASE_FLOOR -> goto end;
           :: else ->
               requested_floor = BASE_FLOOR;
               printf("ELEVATOR GOES TO BASE FLOOR\n");
           fi;
        fi;

        /*@@ perform movement till required floor is reached */
        assert(requested_floor >= 0 && requested_floor < FLOOR_N);
        direction = requested_floor > elevator_pos;

        do
        :: requested_floor == elevator_pos ->
            printf("ELEVATOR REACHED:  %d floor\n", elevator_pos);
            elevator_is_moving = false;

            doors_bus ! OPEN;
            (doors_are_opened);
            if
            :: elevator_pos == user_ordered_floor ->
               /* wait till user is out */
               user_ordered_floor != elevator_pos;
               :: atomic {
                 (user_src_floor == elevator_pos) && user_is_waiting_elev ->
                   user_is_waiting_elev = false
               }
            :: else -> skip;
            fi;
            break; /* go to control */
        :: else ->
            elevator_is_moving = true;
            elevator_pos = elevator_pos + (direction -> 1 : -1);
        od;
    od;
}

/*----------------------------------------------------------------------------*/
/* Doors controller */
active proctype doors_ctl() {
end:do
    :: doors_bus ? CLOSE ->
         atomic{ !(floor_movement) -> doors_are_opened = false }
    :: doors_bus ? OPEN -> doors_are_opened = true;
    od;
}

/*----------------------------------------------------------------------------*/
/* Man controller */

#define PICK_RANDOM_FLOOR \
  if \
  ::true -> rnd = 0;\
  ::true -> rnd = 1;\
  ::true -> rnd = 2;\
  ::true -> rnd = 3;\
  ::true -> rnd = 4;\
  fi;

#define ELEVATOR_IS_INTERACTABLE(floor)\
  (elevator_pos == floor && doors_are_opened)

#define RIDE_TO_DST_FLOOR\
  atomic {\
    printf("MSC: Man enters at floor %d\n", user_src_floor);\
    user_is_waiting_elev = false;\
    floor_movement = false;\
    user_is_inside = true;\
  }\
  \
  atomic {\
    printf("MSC: Man pushes %d inside\n", user_dst_floor);\
    pending_call_request = user_dst_floor;\
    user_ordered_floor = user_dst_floor;\
    printf("DELIVERY PROMISE: %d\n", user_ordered_floor);\
  }\
  atomic {\
    ELEVATOR_IS_INTERACTABLE(user_dst_floor) -> floor_movement = true;\
    printf("SUCCESS: User exits on DST the floor %d\n", user_dst_floor);\
  }\
  atomic {\
    user_ordered_floor = INVALID;\
    floor_movement = false;\
    user_is_inside = false;\
    printf("DELIVERY PROMISE: RESET\n");\
  }


active proctype man() {
  byte rnd = 0;

end:
  do
  :: true ->
      user_detected = false;
      PICK_RANDOM_FLOOR
      user_src_floor = rnd;
      PICK_RANDOM_FLOOR
      user_dst_floor = rnd;

      user_detected = true;
      printf("MSC: Man appears on SRC floor %d want to go to %d\n",
          user_src_floor, user_dst_floor);

      if
      /* @@ Wow, elevator doors are opened, so go inside and ride */
      :: atomic{ ELEVATOR_IS_INTERACTABLE(user_src_floor) ->
          floor_movement = true;}
         RIDE_TO_DST_FLOOR
      :: else ->
          if
          /* ### User pushes button and decides whether he will wait or not */
          :: true ->
              atomic {
                pending_call_request = user_src_floor;
                printf("MSC: Man calls elevator at floor %d\n", user_src_floor);

                /* Decide whether user will be patient enough to wait elevator*/
                if
                ::true -> user_is_waiting_elev = true;
                ::true -> user_is_waiting_elev = false;
                fi
              }

              /*@@ Wait for evelvator or go away */
              if
              /*@ Elevator is here. So go inside */
              :: atomic{ ELEVATOR_IS_INTERACTABLE(user_src_floor) ->
                  floor_movement = true;
              }
              :: else ->
                if
                /*@ Elevator isn't here but usr is patient enough to wait 4 it*/
                :: user_is_waiting_elev ->
                   atomic{
                     ELEVATOR_IS_INTERACTABLE(user_src_floor) ->
                       floor_movement = true;
                   }
                 /*@ Elevator is not here, so go away */
                :: else -> atomic {
                /** in phys world this achvd via mvmnt sensonrs on the flr*/
                  pending_call_request = INVALID;

                  user_ordered_floor = INVALID;
                  floor_movement = false;
                  user_is_waiting_elev = false;
                  printf("MSC: Man leaves SRC floor  %d\n", user_src_floor);
                  goto end;
                }
                fi;
              fi;
              RIDE_TO_DST_FLOOR
          ::true ->
            printf("MSC: %d leaves SRC floor  %d\n", _pid, user_src_floor);
          fi;
      fi;
  od;
}

/*----------------------------------------------------------------------------*/
/* LTL stuff */

#define IS_ACTIVE(x) (x != INVALID)
#define ELEV_ON_BASE (elevator_pos == BASE_FLOOR && !elevator_is_moving)


/* 1. Ride with closed doors */
ltl p1 { [] (elevator_is_moving -> (!doors_are_opened)) }

/* 2. Delivery promise always holds  */
ltl p2 {
  [] (IS_ACTIVE(user_ordered_floor) ->
      (<> ELEVATOR_IS_INTERACTABLE(user_ordered_floor)))
}

/* 4. If movement detected -> doors must be opened */
ltl p4 { always (floor_movement implies doors_are_opened) }


/* 5. If no users -> go to base floor */
/* NB: use user_detected to bound ``man'' process progress,
       ``<> [] !pending_call_request -> ...'' look more reasonable
       but requires fair ``CPU-time'' sharing between processes
*/
ltl p5 {
  (<> [] (!user_detected)) -> (<> [] ELEV_ON_BASE)
}

/* 3. Elevator always goes on user call if user is patient enough*/
ltl p3 {
  [] (user_is_waiting_elev -> (<> ELEVATOR_IS_INTERACTABLE(user_src_floor)))
}

/* Elevator with user doesn't move if button inside is not pressed */
ltl p6 {
   !(<> (user_is_inside &&
         !IS_ACTIVE(user_ordered_floor) &&
         elevator_is_moving))
}


/* ---------- */
/* PLAYGROUND */
