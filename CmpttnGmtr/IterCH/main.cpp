//
//  main.cpp
//  IterConvexHull
//
//  Created by Hatless Fox on 10/13/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <random>
#include <unistd.h>

#include "CHChecker.h"
#include "PointCollection.h"

const unsigned WIDTH = 1024;
const unsigned HEIGHT = 768;

PointCollection points;

void onMouseAction(int button, int state, int x, int y) {
  if (button != GLUT_LEFT_BUTTON || state != GLUT_UP) { return; }
  points.addPoint(x, HEIGHT - y);
  glutPostRedisplay();
}

void initGlState(void){
  glClearColor(1.0, 1.0, 1.0, 1.0);

  glDepthFunc(GL_ALWAYS);
  glEnable(GL_DEPTH_TEST);

  glViewport(0, 0, WIDTH, HEIGHT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluOrtho2D(0, WIDTH, 0, HEIGHT);
  glMatrixMode(GL_MODELVIEW);
}

void printPoints() {
  glColor3f(0.0f, 0.0f, 0.0f);
  glPointSize(3);

  glBegin(GL_POINTS);
  for (auto &point : points.allPoints()) { glVertex2f(point.x, point.y); }
  glEnd();
}


void printConvexHull() {
  glColor3f(1.0f, 0.0f, 0.0f);
  glPointSize(3);

  glBegin(GL_LINE_LOOP);
  for (auto &point : points.convexHull()) {
    glVertex2f(point.x, point.y);
  }
  glEnd();
}


void renderLogic(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//Clear the buffer

  printPoints();
  printConvexHull();

  glutSwapBuffers();
}

void addRandomPoint(int) {
  if (rand() % 13 == 0) {
    points.addPoint(rand() % 600, rand() % 400);
  } else {
    double angle = rand() % 360 / 180.0 * M_PI;
    points.addPoint(300 + std::round(150 * cos(angle)),
                    300 + std::round(150 * sin(angle)));
  }
  glutPostRedisplay();
  glutTimerFunc(400, addRandomPoint, 0);
}

int main(int argc, char * argv[]) {
  if (argc == 3 && std::string("--test") == argv[1]) {
    int points_num = atoi(argv[2]);
    if (points_num < 0) {
      std::cout << "Number of test points must be nonnegative" << std::endl;
      return 0;
    }
    CHChecker ch(points_num);
    std::string msg = ch.validate() ? "Test succeed" : "Test failed";
    std::cout << msg << std::endl;
    return 0;
  }

  srand((unsigned)time(NULL));

  // init GLUT
  glutInit(&argc, (char **)argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

  // create window
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(50, 50);
  glutCreateWindow("IterConvexHull Demo");
  glutMouseFunc(onMouseAction);

  //glutTimerFunc(2000, addRandomPoint, 0);

  GLenum res = glewInit();
  if (res != GLEW_OK) {
    fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
    return 1;
  }

  glutDisplayFunc(renderLogic);
  initGlState();

  glutMainLoop();
  return 0;
}
