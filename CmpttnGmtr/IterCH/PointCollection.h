//
//  PointCollection.h
//  IterConvexHull
//
//  Created by Hatless Fox on 10/13/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef __IterConvexHull__PointCollection__
#define __IterConvexHull__PointCollection__

#include <iostream>
#include <vector>

#include "Point2D.h"
#include "Treap.h"


class PointCollection {
public: // methods
  void addPoint(int x, int y) {
    _points.push_back(Point2D(x, y));

    PointPtr newPointPtr((int)_points.size() - 1, &_points);

    if (_points.size() == 1) {
      _upper = new Treap<PointPtr>(newPointPtr);
      _lower = new Treap<PointPtr>(newPointPtr);
      return;
    } else if (_points.size() == 2) {
      _upper->insert(newPointPtr);
      _lower->insert(newPointPtr);
      return;
    }

    updateConvexHull(newPointPtr);
  }

  std::vector<Point2D> convexHull() {
    std::vector<Point2D> convexHull;

    if (_points.size() < 3) { return convexHull; }

    PointPtr curr = _upper->min();
    while (curr.isValid()) {
      convexHull.push_back(*curr);
      curr = _upper->next(curr);
    }

    curr = _lower->max();
    while (curr.isValid()) {
      convexHull.push_back(*curr);
      curr = _lower->prev(curr);
    }

    return convexHull;
  }

  std::vector<Point2D> const & allPoints() { return _points; }


private: // methods
  bool ccw(Point2D& fst, Point2D& snd, Point2D& thrd) {
    return (snd.x - fst.x) * (thrd.y - fst.y) >
           (thrd.x - fst.x) * (snd.y - fst.y);
  }

  static std::function<bool(bool)> boolId() {
    static auto bool_id = [](bool v) { return v; };
    return bool_id;
  }
  static std::function<bool(bool)> boolNot() {
    static auto bool_not = [](bool v) { return !v; };
    return bool_not;
  }

  void updateBorder(
      PointPtr & newPP,
      PointPtr &startPP,
      Treap<PointPtr> *& border,
      std::function<bool(bool)> isEnough,
      std::function<PointPtr(Treap<PointPtr> *, PointPtr)> extract
  ) {
    PointPtr rmCandidate = startPP;
    PointPtr next = PointPtr::invalid();
    while ((next = extract(border, rmCandidate)).isValid()) {
      if (isEnough(ccw(*newPP, *rmCandidate, *next))) { break; }
      border->remove(rmCandidate);
      rmCandidate = next;
    }
  }

  void updateConvexHull(PointPtr newPP) {
    PointPtr minCHP = _upper->min();
    PointPtr maxCHP = _upper->max();

    if (*newPP <= *minCHP) { // new leftmost
      auto extractor = [](Treap<PointPtr> *b, PointPtr c) { return b->next(c);};
      updateBorder(newPP, minCHP, _upper, boolNot(), extractor);
      updateBorder(newPP, minCHP, _lower, boolId(), extractor);
      _lower->insert(newPP);
      _upper->insert(newPP);
    } else if (*newPP >= *maxCHP) { // new rightmost
      auto extractor = [](Treap<PointPtr> *b, PointPtr c) { return b->prev(c);};
      updateBorder(newPP, maxCHP, _upper, boolId(), extractor);
      updateBorder(newPP, maxCHP, _lower, boolNot(),extractor);
      _lower->insert(newPP);
      _upper->insert(newPP);
    } else { // over existing convex hull
      bool belowMiddleLine = !ccw(*minCHP, *maxCHP, *newPP);

      Treap<PointPtr> *border = belowMiddleLine ? _lower : _upper;

      std::pair<PointPtr, PointPtr> segment = border->findSegment(newPP);
      bool aboveBorder = ccw(*segment.first, *segment.second, *newPP);

      if (belowMiddleLine == aboveBorder) {
        return;  // point is inside existing CH
      }

      updateBorder(newPP, segment.second, border,
                   [&](bool ccw){ return ccw == belowMiddleLine;},
                   [](Treap<PointPtr> *b, PointPtr c) { return b->next(c);});

      updateBorder(newPP, segment.first, border,
                   [&](bool ccw){ return ccw != belowMiddleLine;},
                   [](Treap<PointPtr> *b, PointPtr c) { return b->prev(c);});

      border->insert(newPP);

      if (belowMiddleLine) { _lower = border; }
      else { _upper = border; }
    }
  }

private: // fields
  std::vector<Point2D> _points;

  Treap<PointPtr> *_upper;
  Treap<PointPtr> *_lower;
};


#endif /* defined(__IterConvexHull__PointCollection__) */
