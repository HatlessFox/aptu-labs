//
//  Treap.h
//  IterConvexHull
//
//  Created by Hatless Fox on 10/13/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef IterConvexHull_Treap_h
#define IterConvexHull_Treap_h

#include <memory>

template <typename KeyType>
class Treap {
private: // fwd declarations
  class Node;
private: // type defs
  typedef std::shared_ptr<Node> node_ptr;
public:  // methods

  Treap(KeyType k) { root.reset(new Node(k)); }
  Treap(): root(terminalNode()) {}
  Treap(Node *node): root(node) {}
  Treap(std::shared_ptr<Node> node_ptr): root(node_ptr) {}

  Treap(Treap const &t) { root = t.root; }

  void insert(KeyType &value) {
    std::pair<node_ptr, node_ptr> splitted = split(root, value);

    node_ptr valueNode(new Node(value));
    root = merge(merge(splitted.first, valueNode), splitted.second);
  }

  void remove(KeyType &value) {
    std::pair<node_ptr, node_ptr> spltd = split(root, value, true);
    node_ptr right = split(spltd.second, value, false).second;
    root = merge(spltd.first, right);
  }

  void removeRange(KeyType start, KeyType end) {
    node_ptr left = split(start, this, true).first;
    node_ptr right = split(end, this, false).second;
    root = merge(left, right);
  }

  std::pair<KeyType, KeyType> findSegment(KeyType & key) {
    std::pair<node_ptr, node_ptr> spltd = split(root, key, false);
    std::pair<KeyType, KeyType> res = { max(spltd.first), min(spltd.second) };
    root = merge(spltd);

    return res;
  }

#pragma mark - Specific Key Access Operations

  KeyType max() { return max(root); }
  KeyType min() { return min(root); }

  KeyType next(KeyType &key) {
    std::pair<node_ptr, node_ptr> spltd = split(root, key, false);
    KeyType res = spltd.second ? Treap::min(spltd.second) : KeyType::invalid();
    root = merge(spltd);
    return res;
  }

  KeyType prev(KeyType &key) {
    std::pair<node_ptr, node_ptr> spltd = split(root, key, true);
    KeyType res = spltd.first ? Treap::max(spltd.first) : KeyType::invalid();
    root = merge(spltd);
    return res;
  }

private:  // class
  class Node {
  public: // methods
    Node(KeyType k):
      priority(rand()), key(k), left(terminalNode()), right(terminalNode()) {}
    Node(KeyType k, Node *l, Node *r):
      priority(rand()), key(k), left(l), right(r) {}
  public: // fields
    int priority;
    KeyType key;

    std::shared_ptr<Node> left;
    std::shared_ptr<Node> right;

    inline bool isTerminal() { return !key.isValid(); }
  };

private: // methods

  KeyType max(node_ptr nodeWithMaxKey) {
    if (nodeWithMaxKey->isTerminal()) { return KeyType::invalid(); }
    while (!nodeWithMaxKey->right->isTerminal()) {
      nodeWithMaxKey = nodeWithMaxKey->right;
    }
    return nodeWithMaxKey->key;
  }

  KeyType min(node_ptr nodeWithMinKey) {
    if (nodeWithMinKey->isTerminal()) { return KeyType::invalid(); }
    while (!nodeWithMinKey->left->isTerminal()) {
      nodeWithMinKey = nodeWithMinKey->left;
    }
    return nodeWithMinKey->key;
  }

  static node_ptr terminalNode() {
    static node_ptr terminalNode(new Node(KeyType::invalid(), NULL, NULL));
    return terminalNode;
  }

  std::pair<node_ptr, node_ptr> split(
    std::shared_ptr<Node> root,
    KeyType &splitKey, bool eqIsGtr = true) {

    if (root->isTerminal()) { // base case
      return {terminalNode(), terminalNode()};
    }

    std::pair<node_ptr, node_ptr> splitRes;
    if (splitKey < root->key || (splitKey == root->key && eqIsGtr)) { // go left
      splitRes = split(root->left, splitKey, eqIsGtr);
      root->left = splitRes.second;
      splitRes.second = root;
    } else { // go right
      splitRes = split(root->right, splitKey, eqIsGtr);
      root->right = splitRes.first;
      splitRes.first = root;
    }
    return splitRes;
  }

  node_ptr merge(std::pair<node_ptr, node_ptr> const &treaps) {
    return merge(treaps.first, treaps.second);
  }

  node_ptr merge(node_ptr left, node_ptr right) {
    if (left->isTerminal() || right->isTerminal()) {
      return left->isTerminal() ? right : left;
    } else if (left->priority < right->priority) { // right is upper
      right->left = merge(left, right->left);
      return right;
    } else { // left is upper
      left->right = merge(left->right, right);
      return left;
    }
  }

private: // fields
  std::shared_ptr<Node> root;
};

#endif
