//
//  Point2D.h
//  IterConvexHull
//
//  Created by Hatless Fox on 10/13/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef IterConvexHull_Point2D_h
#define IterConvexHull_Point2D_h

struct Point2D {
public: // methods
  Point2D(int p_x, int p_y): x(p_x), y(p_y) {}
  Point2D(Point2D const & that) { init_with_proto(that); }
  Point2D & operator=(Point2D const & that) {
    init_with_proto(that); return *this;
  }

  bool operator<(Point2D const &that) {
    if (x == that.x) { return y < that.y; }
    return x < that.x;
  }

  bool operator==(Point2D const &that) { return x == that.x && y == that.y; }
  bool operator>=(Point2D const &that) { return !(*this < that); }
  bool operator<=(Point2D const &that) { return *this < that || *this == that; }

public: // fields
  int x;
  int y;
private: // methods
  void init_with_proto(Point2D const &that) {
    x = that.x;
    y = that.y;
  }
};

struct PointPtr {
public: // methods
  PointPtr(int p_ind, std::vector<Point2D> *p_storage):
    ind(p_ind), storage(p_storage) {}
  PointPtr(PointPtr const &that) { init_with_proto(that); }
  PointPtr & operator=(PointPtr const &that) {
    init_with_proto(that); return *this;
  }

  inline Point2D & operator*() const {
    //assert(this->isValid());
    return storage->at(ind);
  }

  static PointPtr & invalid() {
    static PointPtr bad_ptr(-1, NULL);
    return bad_ptr;
  }

  bool isValid() const { return ind >= 0 && storage != 0; }

  bool operator<(PointPtr const &that_ptr) {
    return **this < *that_ptr;
  }

  bool operator==(PointPtr const &that_ptr) {
    return **this == *that_ptr;
  }

  bool operator>(PointPtr const &that_ptr) {
    Point2D &that_pnt = *that_ptr;
    Point2D &this_pnt = **this;

    return !(this_pnt == that_pnt || this_pnt < that_pnt);
  }

private: // methods
  void init_with_proto(PointPtr const &that) {
    ind = that.ind;
    storage = that.storage;
  }
private: // fields
  int ind;
  std::vector<Point2D> *storage;
};

#endif
