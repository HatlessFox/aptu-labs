//
//  CHChecker.h
//  IterConvexHull
//
//  Created by Hatless Fox on 11/26/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef IterConvexHull_CHChecker_h
#define IterConvexHull_CHChecker_h

#include "PointCollection.h"
#include <cstdlib>
#include <vector>

class CHChecker {
public: //methods
  CHChecker(unsigned point_num) {
    while (point_num--) {
      points.addPoint(rand() % 10000, rand() % 10000);
    }
  }

  bool validate() { return checkConvexness() && checkPointsInside(); }

private: //methods

  bool checkConvexness() {
    std::vector<Point2D> ch = points.convexHull();
    size_t ch_size = ch.size();
    for(size_t i = 0, j = ch_size - 1, k = ch_size - 2; i < ch_size; j = i++) {
      if (ccw(ch[k], ch[j], ch[i]) == O_LEFT) { return false; }
      k = j;
    }
    return true;
  }

  bool checkPointsInside() {
    std::vector<Point2D> convex_hull = points.convexHull();
    for (Point2D point : points.allPoints()) {
      if (!checkPointInsidePoly(point, convex_hull)) { return false; }
    }
    return true;
  }

#pragma mark - Point Inside Poly Check

  enum RaySegmentIntersType { RSI_ON_SEG, RSI_INTER, RSI_NO_INTER };

  struct Range {
    Range(int st, int end):lo(st), hi(end) {}
    bool isPoint() { return hi == lo; }
    bool contains(int v) { return (lo > hi) ? (hi < v) && (v <= lo) :
                                              (lo <= v) && (v < hi); }
  private:
    int hi, lo;
  };

  enum Orient { O_LEFT, O_RIGHT, O_COLL };

  Orient ccw(Point2D const & fst, Point2D const & snd, Point2D const & thrd) {
    int64_t area = ((int64_t)snd.x - fst.x) * (thrd.y - fst.y) -
                   (thrd.x - fst.x) * (snd.y - fst.y);
    if (area == 0) { return O_COLL; }
    return area > 0 ? O_LEFT : O_RIGHT;
  }

  RaySegmentIntersType raySegmentInter(Point2D const & seg_st_p,
                                       Point2D const & seg_end_p,
                                       Point2D const & base) {
    Point2D seg_st = seg_st_p, seg_end = seg_end_p;

    Range y_rng(seg_st.y, seg_end.y);
    if (y_rng.contains(base.y)) {
      if (seg_end.y > seg_st.y) {
        Point2D tmp = seg_end;
        seg_end = seg_st;
        seg_st = tmp;
      }
      switch (ccw(base, seg_st, seg_end)) {
        case O_LEFT: return RSI_INTER;
        case O_RIGHT: return RSI_NO_INTER;
        case O_COLL: return RSI_ON_SEG;
      }
    } else {
      if (y_rng.isPoint() && (seg_st.y == base.y)) {
        if (seg_st.x <= base.x && base.x <= seg_end.x) { return RSI_ON_SEG; }
      }
    }

    return RSI_NO_INTER;
  }

  //if point is on border it is treated as inner
  bool checkPointInsidePoly(Point2D point,
                            std::vector<Point2D> const & ch) {
    size_t ch_size = ch.size();
    bool intrs_is_even = true;
    for (size_t j = ch_size - 1, i = 0; i < ch_size; j = i++) {
      switch (raySegmentInter(ch[j], ch[i], point)) {
        case RSI_ON_SEG: return true;
        case RSI_INTER: { intrs_is_even = !intrs_is_even; break;}
        case RSI_NO_INTER:{ break; } //ignore
      }
    }
    return !intrs_is_even;
  }

private: // fields
  PointCollection points;
};

#endif
