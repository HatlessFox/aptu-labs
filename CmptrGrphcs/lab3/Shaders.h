//
//  Shaders.h
//  GR_Lab3
//
//  Created by Hatless Fox on 11/29/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef GR_Lab3_Shaders_h
#define GR_Lab3_Shaders_h

#include <stdexcept>

/**********************/
// Phong Point Light

static const char * UNIFORMS = "\
  uniform mat3 normal_m;        \
  uniform mat4 model_m;         \
  uniform mat4 view_m;          \
  uniform mat4 proj_m;          \
  \
  uniform vec3 ambient_i;       \
  uniform vec3 diffuse_i;       \
  uniform vec3 spec_i;          \
  uniform float spec_lvl;       \
  \
  uniform vec3 light_position;  \
  uniform vec3 light_i;         \
  uniform float light_att;      \
  \
  uniform samplerCube cube_map_tex;     \
";

static const char * PHONG_FUNCTION = "\
  const float FLT_BIAS = 0.00001;\
  void computeColorByPhong(in vec3 norm, in vec3 pos, inout vec3 clr) { \
    vec3 global_light_pos = light_position;\
    vec3 light_dir = normalize(global_light_pos - pos); \
    float brightness = max(dot(norm, light_dir), 0.0);\
    \
    vec3 pos_in_view = vec3(view_m * vec4(pos, 1.0));\
    \
    vec3 light_refl = vec3(normalize(view_m * vec4(reflect(-light_dir, norm), 0.0)));\
    vec3 eye_dir = vec3(normalize(-pos_in_view)); //cam is in (0, 0, 0)\n\
    float spec_fact = pow(max(dot(light_refl, eye_dir), FLT_BIAS), spec_lvl + FLT_BIAS);\
    \
    float dist = length(global_light_pos - pos);\
    float att = 1.0 / (1.0 + light_att * dist * dist);\
    \
    vec3 light_clr = vec3(vec4(light_i, 1.0) * textureCube(cube_map_tex, light_dir));\
    clr = ambient_i + att * light_clr * (brightness * diffuse_i + spec_i * spec_fact);\
  }";

// Flat
std::string phong_point_fl_vtx_sh = std::string() +
 "#version 120\n\
  #extension GL_EXT_gpu_shader4 : enable\n\
  attribute vec3 position; \
  attribute vec3 normal;" + UNIFORMS +
 "varying vec3 vtx_pos;    \
  flat varying vec3 vtx_normal; \
  \
  void main() { \
    vtx_pos = vec3(model_m * vec4(position, 1)); \
    vtx_normal = normal; \
    \
    gl_Position = proj_m * view_m * model_m * vec4(position, 1.0); \
  }";

std::string phong_point_fl_frg_sh = std::string() +
 "#version 120\n\
  #extension GL_EXT_gpu_shader4 : enable\n\
  varying vec3 vtx_pos; \
  flat varying vec3 vtx_normal;" + UNIFORMS + PHONG_FUNCTION +
 "void main() {\
    vec3 normal = normalize(normal_m * vtx_normal); \
    vec3 res_clr = vec3(0);\
    \
    computeColorByPhong(normal, vtx_pos, res_clr);\
    gl_FragColor = vec4(res_clr, 1.0);\
  }";

// Fragment
std::string phong_point_pf_vtx_sh = std::string() +
 "attribute vec3 position; \
  attribute vec3 normal;" + UNIFORMS +
 "varying vec3 vtx_pos;    \
  varying vec3 vtx_normal; \
  \
  void main() { \
    vtx_pos = vec3(model_m * vec4(position, 1.0)); \
    vtx_normal = normal; \
    \
    gl_Position = proj_m * view_m * model_m * vec4(position, 1.0); \
  }";

std::string phong_point_pf_frg_sh = std::string() +
 "varying vec3 vtx_pos; \
  varying vec3 vtx_normal;" + UNIFORMS + PHONG_FUNCTION +
 "void main() {\
    vec3 normal = normalize(normal_m * vtx_normal); \
    vec3 res_clr = vec3(0);\
    \
    computeColorByPhong(normal, vtx_pos, res_clr);\
    gl_FragColor = vec4(res_clr, 1.0);\
  }";

// Vertex
std::string phong_point_pv_vtx_sh = std::string() +
 "attribute vec3 position; \
  attribute vec3 normal;   \
  varying vec3 color;" + UNIFORMS + PHONG_FUNCTION +
 "void main() { \
    vec3 vtx_pos = vec3(model_m * vec4(position, 1)); \
    vec3 norm = normalize(normal_m * normal); \
    \
    computeColorByPhong(norm, vtx_pos, color);\
    gl_Position = proj_m * view_m * model_m * vec4(position, 1.0); \
}";

static const char* phong_point_pv_frg_sh = "\
  varying vec3 color;    \
  \
  void main() {\
    gl_FragColor = vec4(color, 1.0);\
  }";

/**********************/
// Blinn-Phong Point Light

static const char * BLINN_PHONG_FUNCTION = "\
  const float FLT_BIAS = 0.00001;\
  void computeColorByBlinnPhong(in vec3 norm, in vec3 pos, inout vec3 clr) { \
    vec3 global_light_pos = light_position;\
    vec3 light_dir = normalize(global_light_pos - pos); \
    float brightness = max(dot(norm, light_dir), 0.0);\
    \
    vec3 pos_in_view = vec3(view_m * vec4(pos, 1.0));\
    \
    vec3 eye_dir = normalize(-pos_in_view); \
    vec3 h_vect = normalize(vec3(view_m * vec4(light_dir, 0.0)) + eye_dir);\
    float spec_fact = pow(max(dot(h_vect, norm), FLT_BIAS), spec_lvl + FLT_BIAS);\
    \
    float dist = length(global_light_pos - pos);\
    float att = 1.0 / (1.0 + light_att * dist * dist);\
    \
    vec3 light_clr = vec3(vec4(light_i, 1.0) * textureCube(cube_map_tex, light_dir));\
    clr = ambient_i + att * light_clr * (brightness * diffuse_i + spec_i * spec_fact);\
  }";

// Fragment
std::string blinn_phong_point_pf_frg_sh = std::string() +
 "varying vec3 vtx_pos; \
  varying vec3 vtx_normal;" + UNIFORMS + BLINN_PHONG_FUNCTION +
 "void main() {\
    vec3 normal = normalize(normal_m * vtx_normal); \
    vec3 clr = vec3(0);\
    \
    computeColorByBlinnPhong(normal, vtx_pos, clr); \
    gl_FragColor = vec4(clr, 1.0);\
  }";

//Flat
std::string blinn_phong_point_fl_frg_sh = std::string() +
 "#version 120\n\
  #extension GL_EXT_gpu_shader4 : enable\n\
  varying vec3 vtx_pos; \
  flat varying vec3 vtx_normal;" + UNIFORMS + BLINN_PHONG_FUNCTION +
 "void main() {\
    vec3 normal = normalize(normal_m * vtx_normal); \
    vec3 clr = vec3(0);\
    \
    computeColorByBlinnPhong(normal, vtx_pos, clr); \
    gl_FragColor = vec4(clr, 1.0);\
  }";

// Vertex
std::string blinn_phong_point_pv_vtx_sh = std::string() +
 "attribute vec3 position; \
  attribute vec3 normal;   \
  varying vec3 color;      " + UNIFORMS + BLINN_PHONG_FUNCTION +
 "void main() { \
    vec3 vtx_pos = vec3(model_m * vec4(position, 1.0)); \
    vec3 norm = normalize(normal_m * normal); \
    \
    computeColorByBlinnPhong(norm, vtx_pos, color); \
    gl_Position = proj_m * view_m * model_m * vec4(position, 1.0); \
  }";

//------------------------------------------------------------------------------
// Sphere shader

static const char* sphere_vtx_sh = "\
  attribute vec3 position;      \
  uniform mat4 model_m;         \
  uniform mat4 view_m;          \
  uniform mat4 proj_m;          \
  uniform float radius;         \
  \
  void main() {\
    gl_Position = proj_m * view_m * model_m * vec4(radius * position, 1.0);\
  }";

static const char* sphere_frg_sh = "\
  \
  void main() { gl_FragColor = vec4(1, 1, 0, 1.0); }";


#pragma mark - Util classes

class Shader {
public: // methods
  Shader(const char* shader_text, GLenum shader_type) {
    m_shader_id = glCreateShader(shader_type);
    
    if (m_shader_id == 0) {
      throw std::runtime_error("Error creating shader type " + std::to_string(shader_type));
    }
    
    GLint len = (GLint)strlen(shader_text);
    glShaderSource(m_shader_id, 1, &shader_text, &len);
    glCompileShader(m_shader_id);
    GLint success;
    glGetShaderiv(m_shader_id, GL_COMPILE_STATUS, &success);
    if (!success) {
      GLchar log[1024];
      glGetShaderInfoLog(m_shader_id, 1024, NULL, log);
      std::cerr << log << std::endl;
      glDeleteShader(m_shader_id);
      throw std::runtime_error("Error compiling shader type " + std::to_string(shader_type));
    }
  }
  
  ~Shader() { glDeleteShader(m_shader_id); }
  
  inline GLuint descr() const { return m_shader_id; }
  
private: // methods
  Shader(const Shader &);
  Shader & operator=(const Shader &);
private: // fields
  GLuint m_shader_id;
};

class Program {
public: // methods
  Program(const char * vtx_sh, const char * frag_sh) {
    m_prog_id = glCreateProgram();
    if (m_prog_id == 0) { throw std::runtime_error("Error creating shader program\n"); }
    
    Shader vtx(vtx_sh, GL_VERTEX_SHADER);
    Shader frg(frag_sh, GL_FRAGMENT_SHADER);
    
    glAttachShader(m_prog_id, vtx.descr());
    glAttachShader(m_prog_id, frg.descr());
    
    glLinkProgram(m_prog_id);
    checkProgramErrors(m_prog_id, GL_LINK_STATUS);
  }
  
  inline void use() { glUseProgram(m_prog_id); }
  inline void stopUsing() { glUseProgram(0); }
  
  ~Program() {
    if (m_prog_id != 0) { glDeleteProgram(m_prog_id); }
  }
  
  inline GLuint desc() { return m_prog_id; }
  
  
  GLint attLc(const GLchar* attribute_name) const {
    GLint attrib_loc = glGetAttribLocation(m_prog_id, attribute_name);
    if(attrib_loc == -1) {
      throw std::runtime_error(std::string("Program attribute not found: ") +
                               attribute_name);
    }
    return attrib_loc;
  }
  
  GLint uniLc(const GLchar* uniform_name) const {
    GLint uniform_loc = glGetUniformLocation(m_prog_id, uniform_name);
    if(uniform_loc == -1) {
      throw std::runtime_error(std::string("Program uniform not found: ") +
                               uniform_name);
    }
    return uniform_loc;
  }
  
private: // methods
  void checkProgramErrors(GLint shaderProgram, GLenum checkParam) {
    GLint success = 0;
    glGetProgramiv(shaderProgram, checkParam, &success);
    if (!success) {
      GLchar log[1024];
      glGetProgramInfoLog(shaderProgram, sizeof(log), NULL, log);
      std::cerr << log << std::endl;
      
      glDeleteProgram(m_prog_id);
      throw std::runtime_error("Program check error");
    }
  }
  
private: // field
  GLuint m_prog_id;
};

#endif
