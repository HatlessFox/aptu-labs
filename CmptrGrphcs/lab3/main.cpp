//
//  main.cpp
//
//  Created by Hatless Fox on 9/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <algorithm>
#include <float.h>
#include <map>
#include <vector>
#include <AntTweakBar.h>
#include <ImageMagick/Magick++.h>

#include "ObjFileLoader.h"
#include "Shaders.h"


float g_fov = 20.0;
float g_near = .1;
float g_far = 100.0;

glm::quat g_cam_orient;
glm::quat g_model_orient;

glm::vec3 g_light_color(1.0f, 1.0f, 1.0f);
float g_light_x(5.0f);
float g_light_y(5.0f);
float g_light_z(5.0f);
float g_light_att = 0;

float g_mtrl_amb[] = {0.1f, 0.1f, 0.1f};
float g_mtrl_diff[] = {0.6f, 0.6f, 0.6f};
float g_mtrl_spec[] = {0.3f, 0.0f, 0.0f};
float g_mtrl_spec_lvl = 10;

float aspectRatio;

int g_display_mode;

struct ModeData {
  GLuint vbo;
  GLuint ibo;
  size_t elems_cnt;
  std::shared_ptr<Program> light_prog;
};

std::map<int, ModeData> g_modes;
ModeData light_sphere;
GLuint g_cube_map;

void onReshape(int w, int h) {
  aspectRatio = w * 1.0 / h;
  glViewport(0, 0, w, h);
  TwWindowSize(w, h);
}

#pragma mark - GL initializers

void initGlState(){
  glClearColor(0.0, 0.0, 0.0, 0.0);
  
  glEnable(GL_DEPTH_TEST);
}

void loadCubeMapTexture() {
  std::vector<std::string> file_names;
  file_names.push_back("./hatiko_fur.jpg");
  file_names.push_back("./giraffe_fur.jpg");
  file_names.push_back("./black_leo_fur.jpeg");
  file_names.push_back("./foxy_fur.jpeg");
  file_names.push_back("./spots_fur.jpg");
  file_names.push_back("./artificial_fur.jpg");
  
  glGenTextures(1, &g_cube_map);
  glBindTexture(GL_TEXTURE_CUBE_MAP, g_cube_map);
  glActiveTexture(GL_TEXTURE0);
  for (size_t i = 0; i < file_names.size(); ++i) {
    std::shared_ptr<Magick::Image> img(new Magick::Image(file_names[i]));
    Magick::Blob blob;
    img->resize(Magick::Geometry(512, 512));
    img->flip();
    img->write(&blob, "RGBA");

    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + (int)i, 0, GL_RGB8, (GLsizei)img->columns(), (GLsizei)img->rows(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE, blob.data());
  
  }
  
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}


void pushPlaneCoord(std::vector<float> &st, std::vector<float> *dest) {
  for (int i = 0; i < 4; ++i) {
    for (size_t j = 0; j < st.size(); ++j) {
      if (st[j] == 0) { dest->push_back(st[j]); }
      else {
        int bit = fabs(fabs(st[j]) - 1) < FLT_EPSILON ? 0 : 1;
        dest->push_back(st[j] * ((i & 1 << bit) ? -1 : 1));
      }
    }
  }
}

int genMiddle(size_t p1i, size_t p2i, std::vector<float> *vtxs, std::map<int64_t, int> *cache) {
  int64_t key = ((uint64_t)p1i << 32) + p2i; //traversal always clockwise
  if (cache->count(key)) { return cache->at(key); }
  
  size_t new_ind = vtxs->size() / 3;
  cache->operator[](key) = (int)new_ind;
  
  float x = (vtxs->at(p1i * 3) + vtxs->at(p2i * 3)) / 2;
  float y = (vtxs->at(p1i * 3 + 1) + vtxs->at(p2i * 3 + 1)) / 2;
  float z = (vtxs->at(p1i * 3 + 2) + vtxs->at(p2i * 3 + 2)) / 2;
  float len = sqrtf(x*x + y*y + z*z);
  vtxs->push_back(x / len);
  vtxs->push_back(y / len);
  vtxs->push_back(z / len);
  
  return (int)new_ind;
}

void generateAndInitSphere(ModeData *mode) {
  std::vector<float> vtxs;
  std::vector<int> inds;
  
  //create base icosahedron
  float gr = (1 + sqrt(5)) / 2;
  std::vector<float> base = { -1, gr, 0 };
  for (int i = 0; i < 3; i++) {
    pushPlaneCoord(base, &vtxs);
    std::rotate(base.begin(), base.begin() + 2, base.end());
  }
  
  //normalize vtxes
  for (int p_ind = 0; p_ind < vtxs.size(); p_ind += 3) {
    float len = sqrtf(vtxs[p_ind] * vtxs[p_ind] +
                      vtxs[p_ind + 1] * vtxs[p_ind + 1] +
                      vtxs[p_ind + 2] * vtxs[p_ind + 2]);
    vtxs[p_ind] /= len;
    vtxs[p_ind + 1] /= len;
    vtxs[p_ind + 2] /= len;
  }
  
  inds = { 0, 11, 5,/**/0,  5,  1,/**/ 0,  1,  7,/**/ 0, 7, 10,/**/0, 10, 11,/**/
    1,  5, 9,/**/5, 11,  4,/**/11, 10,  2,/**/10, 7,  6,/**/7,  1,  8,/**/
    3,  9, 4,/**/3,  4,  2,/**/ 3,  2,  6,/**/ 3, 6,  8,/**/3,  8,  9,/**/
    4,  9, 5,/**/2,  4, 11,/**/ 6,  2, 10,/**/ 8, 6,  7,/**/9,  8, 1};
  
  //enhanse icosahedron to be more like sphere
  for (int ench_iter = 0; ench_iter < 3; ++ench_iter) {
    std::vector<int> tmp_inds;
    std::map<int64_t, int> mid_cache;
    
    for (int face_ind = 0; face_ind < inds.size(); face_ind += 3) {
      int p1 = inds[face_ind]; int p2 = inds[face_ind + 1]; int p3 = inds[face_ind + 2];
      
      int mid01 = genMiddle(p1, p2, &vtxs, &mid_cache);
      int mid12 = genMiddle(p2, p3, &vtxs, &mid_cache);
      int mid20 = genMiddle(p3, p1, &vtxs, &mid_cache);
      
      tmp_inds.push_back(p1); tmp_inds.push_back(mid01); tmp_inds.push_back(mid20);
      tmp_inds.push_back(p2); tmp_inds.push_back(mid12); tmp_inds.push_back(mid01);
      tmp_inds.push_back(p3); tmp_inds.push_back(mid20); tmp_inds.push_back(mid12);
      tmp_inds.push_back(mid01); tmp_inds.push_back(mid12); tmp_inds.push_back(mid20);
    }
    
    inds = tmp_inds;
  }
  
  
  glGenBuffers(1, &(mode->vbo));
  glBindBuffer(GL_ARRAY_BUFFER, mode->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vtxs.size(), &vtxs[0], GL_STATIC_DRAW);
  
  glGenBuffers(1, &(mode->ibo));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mode->ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLint) * inds.size(), &inds[0], GL_STATIC_DRAW);
  
  mode->elems_cnt = (GLuint)inds.size();
}


void initPhongFragMode() {
  const int SCALE_FACTOR = 10;
  
  ModeData data;
  
  data.light_prog.reset(new Program(phong_point_pf_vtx_sh.c_str(), phong_point_pf_frg_sh.c_str()));
  
  ObjFileLoader ldr;
//    ldr.loadFromFile("./bunny_n.obj");
  ldr.loadFromFile("./bunny_n.obj");
  
  //merge coords with normals
  assert(ldr.loaded_vtx_coords_size() == ldr.loaded_vtx_normals_size());
  
  std::vector<float> merged_data;
  std::vector<int> elems_data;
  float *coords = ldr.loaded_vtx_coords();
  
  //find center of object
  #define HANDLE_COORD(min, val, max)\
    if (val > max) { max = val; }\
    if (min > val) { min = val; }
  
  float *coords_ptr = coords;
  float xMax = -10e6, xMin = 10e6, yMax = -10e6, yMin = 10e6, zMax = -10e6, zMin = 10e6;
  for (size_t i = 0; i < ldr.loaded_vtx_coords_size() / sizeof(float); i += 3) {
    float x = *coords_ptr++;
    HANDLE_COORD(xMin, x, xMax)
    float y = *coords_ptr++;
    HANDLE_COORD(yMin, y, yMax)
    float z = *coords_ptr++;
    HANDLE_COORD(zMin, z, zMax)
  }
  #undef HANDLE_COORD

  float delta_x = -(xMax + xMin) / 2,
        delta_y = -(yMax + yMin) / 2,
        delta_z = -(zMax + zMin) / 2;
  
  float *normals = ldr.loaded_vtx_normals();
  int elems_cnt = 0;
  for (size_t i = 0; i < ldr.loaded_vtx_coords_size() / sizeof(float); i += 3, elems_cnt++) {
    merged_data.push_back(SCALE_FACTOR * (*coords++ + delta_x));
    merged_data.push_back(SCALE_FACTOR * (*coords++ + delta_y));
    merged_data.push_back(SCALE_FACTOR * (*coords++ + delta_z));
    for (size_t j = 0; j < 3; ++j) { merged_data.push_back(*normals++); }
  }

  int *faces_ptr = ldr.loaded_faces();
  for (size_t i = 0;
       i < ldr.loaded_faces_size() / sizeof(int);
       i += 1) {
    elems_data.push_back(*faces_ptr++);
  }
  
  //**load plane
  #define LOAD_PLANE_COORD(x, z) \
    merged_data.push_back((x - delta_x) * SCALE_FACTOR);\
    merged_data.push_back((yMin + delta_y) * SCALE_FACTOR);\
    merged_data.push_back((z - delta_z) * SCALE_FACTOR);\
    merged_data.push_back(0.0);\
    merged_data.push_back(1.0);\
    merged_data.push_back(0.0);
  
  LOAD_PLANE_COORD(-0.1, -0.1)
  LOAD_PLANE_COORD(-0.1,  0.1)
  LOAD_PLANE_COORD( 0.1,  0.1)
  LOAD_PLANE_COORD( 0.1, -0.1)
  
  #undef LOAD_PLANE_COORD
  
  elems_data.push_back(elems_cnt);
  elems_data.push_back(elems_cnt + 1);
  elems_data.push_back(elems_cnt + 2);
  elems_data.push_back(elems_cnt);
  elems_data.push_back(elems_cnt + 2);
  elems_data.push_back(elems_cnt + 3);
  
  glGenBuffers(1, &data.vbo);
  glBindBuffer(GL_ARRAY_BUFFER, data.vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(float) * merged_data.size(),
               &merged_data[0], GL_STATIC_DRAW);
  
  glGenBuffers(1, &data.ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * elems_data.size(),
               &elems_data[0], GL_STATIC_DRAW);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  data.elems_cnt = elems_data.size();
  
  g_modes[2] = data;
}

void initPhongVtxMode() {
  ModeData data = g_modes[2];
  
  data.light_prog.reset(new Program(phong_point_pv_vtx_sh.c_str(),
                                    phong_point_pv_frg_sh));
  g_modes[1] = data;
}

void initPhongFlatMode() {
  ModeData data = g_modes[2];
  
  data.light_prog.reset(new Program(phong_point_fl_vtx_sh.c_str(),
                                    phong_point_fl_frg_sh.c_str()));
  g_modes[0] = data;
}

void initBlinnPhongFragMode() {
  ModeData data = g_modes[2];
  
  data.light_prog.reset(new Program(phong_point_pf_vtx_sh.c_str(),
                                    blinn_phong_point_pf_frg_sh.c_str()));
  g_modes[6] = data;
}

void initBlinnPhongVtxMode() {
  ModeData data = g_modes[2];
  
  data.light_prog.reset(new Program(blinn_phong_point_pv_vtx_sh.c_str(),
                                    phong_point_pv_frg_sh));
  g_modes[5] = data;
}

void initBlinnPhongFlatMode() {
  ModeData data = g_modes[2];
  
  data.light_prog.reset(new Program(phong_point_fl_vtx_sh.c_str(),
                                    blinn_phong_point_fl_frg_sh.c_str()));
  
  g_modes[4] = data;
}

void initDisplayData() {
  aspectRatio = 640.0 / 480;

  initPhongFragMode();
  initBlinnPhongFragMode();
  initPhongVtxMode();
  initBlinnPhongVtxMode();
  initPhongFlatMode();
  initBlinnPhongFlatMode();
  generateAndInitSphere(&light_sphere);
  light_sphere.light_prog.reset(new Program(sphere_vtx_sh, sphere_frg_sh));
  loadCubeMapTexture();
  
  g_display_mode = 2;
}

#pragma mark - Renderer

void renderBunny(glm::mat4 const & proj, glm::mat4 const & view) {
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
  const ModeData &data = g_modes[g_display_mode];
  
  data.light_prog->use();
  
  glm::mat4 model_matr =  glm::mat4_cast(g_model_orient);
  glm::mat3 normal_matr = glm::inverseTranspose(glm::mat3(model_matr));
  
  glUniformMatrix3fv(data.light_prog->uniLc("normal_m"), 1.0f, GL_FALSE, glm::value_ptr(normal_matr));
  glUniformMatrix4fv(data.light_prog->uniLc("proj_m"), 1.0f, GL_FALSE, glm::value_ptr(proj));
  glUniformMatrix4fv(data.light_prog->uniLc("view_m"), 1.0f, GL_FALSE, glm::value_ptr(view));
  glUniformMatrix4fv(data.light_prog->uniLc("model_m"), 1.0f, GL_FALSE, glm::value_ptr(model_matr));
  
  glUniform3fv(data.light_prog->uniLc("light_position"), 1, glm::value_ptr(glm::vec3(g_light_x, g_light_y, g_light_z)));
  glUniform3fv(data.light_prog->uniLc("light_i"), 1, glm::value_ptr(g_light_color));
  glUniform1f(data.light_prog->uniLc("light_att"), g_light_att);
  
  //material setup
  glUniform3fv(data.light_prog->uniLc("ambient_i"), 1, g_mtrl_amb);
  glUniform3fv(data.light_prog->uniLc("diffuse_i"), 1, g_mtrl_diff);
  glUniform3fv(data.light_prog->uniLc("spec_i"), 1, g_mtrl_spec);
  glUniform1f(data.light_prog->uniLc("spec_lvl"), g_mtrl_spec_lvl);
  
  // cube map
  glUniform1f(data.light_prog->uniLc("cube_map_tex"), g_cube_map);
  
  glDrawElements(GL_TRIANGLES, (GLint)data.elems_cnt, GL_UNSIGNED_INT, 0);
  
  data.light_prog->stopUsing();
}

void renderSphere(glm::mat4 const & proj, glm::mat4 const & view) {
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  
  const ModeData &data = light_sphere;
  
  data.light_prog->use();
  
  glm::mat4 model_matr = glm::translate(glm::mat4(), glm::vec3(g_light_x, g_light_y, g_light_z));
  
  glUniformMatrix4fv(data.light_prog->uniLc("proj_m"), 1.0f, GL_FALSE, glm::value_ptr(proj));
  glUniformMatrix4fv(data.light_prog->uniLc("view_m"), 1.0f, GL_FALSE, glm::value_ptr(view));
  glUniformMatrix4fv(data.light_prog->uniLc("model_m"), 1.0f, GL_FALSE, glm::value_ptr(model_matr));
  
  // 1/(1 + l_a*r*r) = 1/255 => r = sqrt(254 / l_a);
  float radius = g_light_att ? sqrt(254 / g_light_att) : 3;
  glUniform1f(data.light_prog->uniLc("radius"), radius);
  glDrawElements(GL_TRIANGLES, (GLint)data.elems_cnt, GL_UNSIGNED_INT, 0);
  
  data.light_prog->stopUsing();
}


void renderLogic(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glm::mat4 proj = glm::perspective(g_fov, aspectRatio, g_near, g_far);
  glm::mat4 view_matr = glm::translate(glm::mat4(), glm::vec3(0.8f, 0.0f, -7.0f)) * glm::mat4_cast(g_cam_orient);
  
  //** Model render
  const ModeData &data = g_modes[g_display_mode];
  
  glBindBuffer(GL_ARRAY_BUFFER, data.vbo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.ibo);
  glEnableVertexAttribArray(data.light_prog->attLc("position"));
  glEnableVertexAttribArray(data.light_prog->attLc("normal"));
  
  GLsizei stride = 6 * sizeof(GLfloat);
  glVertexAttribPointer(data.light_prog->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, NULL);
  glVertexAttribPointer(data.light_prog->attLc("normal"), 3, GL_FLOAT, GL_TRUE, stride, (const GLvoid *)(3*sizeof(GLfloat)));
  
  renderBunny(proj, view_matr);
  
  glDisableVertexAttribArray(data.light_prog->attLc("normal"));
  glDisableVertexAttribArray(data.light_prog->attLc("position"));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  //** Light Sphere render
  glBindBuffer(GL_ARRAY_BUFFER, light_sphere.vbo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_sphere.ibo);
  glEnableVertexAttribArray(light_sphere.light_prog->attLc("position"));
  
  GLsizei light_sph_stride = 3 * sizeof(GLfloat);
  glVertexAttribPointer(light_sphere.light_prog->attLc("position"), 3, GL_FLOAT, GL_FALSE, light_sph_stride, NULL);
  
  renderSphere(proj, view_matr);
  
  glDisableVertexAttribArray(light_sphere.light_prog->attLc("position"));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  
  TwDraw();
  
  glutSwapBuffers();
}

#pragma mark - Main and utils

#pragma mark - GLUT Input handlers

void mousebuttonfun(int glutButton, int glutState, int mouseX, int mouseY) {
  TwEventMouseButtonGLUT(glutButton, glutState, mouseX, mouseY);
  glutPostRedisplay();
}
void mousemotionfun(int mouseX, int mouseY) {
  TwEventMouseMotionGLUT(mouseX, mouseY);
  glutPostRedisplay();
}
void keyboardfun(unsigned char glutKey, int mouseX, int mouseY) {
  TwEventKeyboardGLUT(glutKey, mouseX, mouseY);
  glutPostRedisplay();
}
void specialfun(int glutKey, int mouseX, int mouseY) {
  TwEventSpecialGLUT(glutKey, mouseX, mouseY);
  glutPostRedisplay();
}

void setupGUI() {
  // Initialize AntTweakBar
  TwInit(TW_OPENGL, NULL);
  
  glutMouseFunc(mousebuttonfun);
  glutMotionFunc(mousemotionfun);
  glutPassiveMotionFunc(mousemotionfun);
  glutKeyboardFunc(keyboardfun);
  glutSpecialFunc(specialfun);
  TwGLUTModifiersFunc(glutGetModifiers);
  
  // Create a tweak bar
  
  TwBar *bar = TwNewBar("Tweaks");
  TwDefine(" Tweaks size='300 450' ");
  
  TwAddVarRW(bar, "FOV", TW_TYPE_FLOAT, &g_fov, "min=0.01 max=100 step=1 keyIncr=z keyDecr=Z group='View Setup'");
  TwAddVarRW(bar, "Near", TW_TYPE_FLOAT, &g_near, "min=0.01 max=100 step=.5 keyIncr=u keyDecr=U group='View Setup'");
  TwAddVarRW(bar, "Far", TW_TYPE_FLOAT, &g_far, "min=0.01 max=100 step=.5 keyIncr=e keyDecr=E group='View Setup'");
  
  TwAddVarRW(bar, "Cam Rotation", TW_TYPE_QUAT4F, (void *)glm::value_ptr(g_cam_orient), "opened=true group='View Setup'");
  TwAddVarRW(bar, "Model Rotation", TW_TYPE_QUAT4F, (void *)glm::value_ptr(g_model_orient), "opened=true group='View Setup'");

  TwAddVarRW(bar, "Color", TW_TYPE_COLOR3F, glm::value_ptr(g_light_color), "group='Light Setup'");
  
  TwAddVarRW(bar, "Pos X", TW_TYPE_FLOAT, &g_light_x, "group='Position' ");
  TwAddVarRW(bar, "Pos Y", TW_TYPE_FLOAT, &g_light_y, "group='Position' ");
  TwAddVarRW(bar, "Pos Z", TW_TYPE_FLOAT, &g_light_z, "group='Position' ");
  TwAddVarRW(bar, "Attenuation Coeff", TW_TYPE_FLOAT, &g_light_att, "min=0.0 max=100 step=.01 group='Light Setup' ");
  TwDefine("Tweaks/Position group='Light Setup' opened=false");
  
  //attenuation by dist
  
  TwAddVarRW(bar, "Ambient", TW_TYPE_COLOR3F, g_mtrl_amb, "group='Material Setup'");
  TwAddVarRW(bar, "Diffuse", TW_TYPE_COLOR3F, g_mtrl_diff, "group='Material Setup'");
  TwAddVarRW(bar, "Specular", TW_TYPE_COLOR3F, g_mtrl_spec, "group='Material Setup'");
  TwAddVarRW(bar, "Specular Lvl", TW_TYPE_FLOAT, &g_mtrl_spec_lvl, "group='Material Setup' ");
}

void handlePhSelection(int m) { g_display_mode = m; }
void handleBPhSelection(int m) { g_display_mode = 4 + m; }

void createMenu() {
  int phong_submenu = glutCreateMenu(handlePhSelection);
  glutAddMenuEntry("Flat", 0);
  glutAddMenuEntry("Vtx", 1);
  glutAddMenuEntry("Frag", 2);
  
  int blinnphong_submenu = glutCreateMenu(handleBPhSelection);
  glutAddMenuEntry("Flat", 0);
  glutAddMenuEntry("Vtx", 1);
  glutAddMenuEntry("Frag", 2);
  
  glutCreateMenu(0);
  glutAddSubMenu("Phong", phong_submenu);
  glutAddSubMenu("Blinn-Phong", blinnphong_submenu);
  
  glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, const char * argv[]) {
  // init GLUT
  glutInit(&argc, (char **)argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  
  // create window
  glutInitWindowSize(800, 600);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("Lab 3");
  createMenu();
  
  glutDisplayFunc(renderLogic);
  glutReshapeFunc(onReshape);
  
  setupGUI();
  
  GLenum res = glewInit();
  if (res != GLEW_OK) {
    fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
    return 1;
  }
  
  initGlState();
  initDisplayData();
  
  glutMainLoop();
  return 0;
}
