//
//  Shaders.h
//  test_Graphics
//
//  Created by Hatless Fox on 10/15/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef test_Graphics_Shaders_h
#define test_Graphics_Shaders_h

/**********************/
// Particle updater
static const char* upd_vsp = "                                                 \
  #version 330 \n                                                              \
  layout (location = 0) in vec3 init_vel;                                      \
  layout (location = 1) in float splash_ind;                                   \
  layout (location = 2) in vec3 position;                                      \
  layout (location = 3) in vec3 velocity;                                      \
  layout (location = 4) in float time_shift;                                   \
                                                                               \
  uniform float time_mult = 1.0;                                               \
                                                                               \
  out vec3 init_vel_out;                                                       \
  out float splash_ind_out;                                                    \
  out vec3 position_out;                                                       \
  out vec3 velocity_out;                                                       \
  out float time_shift_out;                                                    \
                                                                               \
  void main() {                                                                \
    init_vel_out = init_vel;                                                   \
    splash_ind_out =  splash_ind;                                              \
    position_out = position + velocity * (.25 * time_mult);                    \
    velocity_out = velocity + vec3(0.0, -0.049, 0.0) * (.25 * time_mult);      \
    time_shift_out = max(0.0, time_shift - 1.0);                               \
                                                                               \
    if (position_out.y < -2.0 || time_shift_out > 0.0) {                       \
      splash_ind_out =  mod(splash_ind + 1, 16);                               \
      position_out = vec3(0.0, 0.0, 0.0);                                      \
      velocity_out = init_vel;                                                 \
    }                                                                          \
    if (time_shift_out > 0.0) { position_out = vec3(0.0, -1000.0, 0.0); }      \
  }";


/**********************/
// Background rendering

static const char* bg_vsp = "                                                  \
  #version 330 \n                                                              \
  layout (location = 0) in vec3 position;                                      \
  layout (location = 1) in vec2 tex_coord;                                     \
                                                                               \
  out vec2 frag_tex_c;                                                         \
  uniform mat4 worldMatrix;                                                    \
                                                                               \
  void main() {                                                                \
    frag_tex_c = tex_coord;                                                    \
    gl_Position = worldMatrix * vec4(position, 1.0);                           \
  }";

static const char* bg_fsp = "                                                  \
  #version 330 \n                                                              \
  uniform sampler2D tex;                                                       \
                                                                               \
  in vec2 frag_tex_c;                                                          \
  layout (location = 0) out vec4 color;                                        \
  void main() {                                                                \
    color = texture(tex, frag_tex_c.st);                                       \
  }";


/**********************/
// Particle rendering

static const char* vsp = "\
  #version 330 \n\
  layout (location = 0) in vec3 init_vel;                                      \
  layout (location = 1) in float splash_ind;                                   \
  layout (location = 2) in vec3 position;                                      \
  layout (location = 3) in vec3 velocity;                                      \
  layout (location = 4) in float time_shift;                                   \
  \
  uniform mat4 model_m;\
  \
  out float splash_ind_gin;\
  \
  void main() {\
    splash_ind_gin = splash_ind;\
    \
    gl_Position = model_m * vec4(position, 1.0);\
  }";

static const char* gsp = "\
  #version 330 \n\
  layout (points) in;\
  layout (triangle_strip, max_vertices = 4) out;\
  \
  in float splash_ind_gin[1];\
  \
  uniform float splash_scale;\
  uniform mat4 view_m;\
  uniform mat4 proj_m;\
  \
  out float splash_ind_fin;\
  out vec2 frag_tex_c;\
  \
  void main() {\
    vec3 pos      = gl_in[0].gl_Position.xyz;\
    vec4 view_pos = view_m * vec4(pos, 1.0);\
    vec4 cam_pos  = vec4(0.0, 0.0, 0.0, 1.0);\
    vec4 cam_up   = vec4(0.0, 1.0, 0.0, 0.0);\
    \
    vec4 to_cam = normalize(cam_pos - view_pos);\
    vec4 right = vec4(normalize(cross(vec3(cam_up), vec3(to_cam))), 0.0);\
    \
    view_pos -= .5 * right * splash_scale;\
    view_pos.y -= .5 * splash_scale;\
    gl_Position =  proj_m * view_pos;\
    frag_tex_c = vec2(0.0, 0.0);\
    splash_ind_fin = splash_ind_gin[0];\
    EmitVertex();\
    \
    view_pos.y += 1.0 * splash_scale;\
    gl_Position =  proj_m * view_pos;\
    frag_tex_c = vec2(0.0, 1.0);\
    splash_ind_fin = splash_ind_gin[0];\
    EmitVertex();\
    \
    view_pos.y -= 1.0 * splash_scale;\
    view_pos += right * splash_scale;\
    gl_Position =  proj_m * view_pos;\
    frag_tex_c = vec2(1.0, 0.0);\
    splash_ind_fin = splash_ind_gin[0];\
    EmitVertex();\
    \
    view_pos.y += 1.0 * splash_scale;\
    gl_Position =  proj_m * view_pos;\
    frag_tex_c = vec2(1.0, 1.0);\
    splash_ind_fin = splash_ind_gin[0];\
    EmitVertex();\
    \
    EndPrimitive();\
  }";


static const char* fsp = "\
  #version 330 \n\
  uniform sampler2D tex;\
  uniform sampler2D background_tex;\
  uniform sampler1D noise_tex;\
  \
  uniform int atlas_x;\
  uniform int atlas_y;\
  \
  uniform float w_width;\
  uniform float w_height;\
  \
  in float splash_ind_fin;\
  in vec2 frag_tex_c;\
  \
  out vec4 frag_color;\
  void main() {\
    \
    int splash_i = int(splash_ind_fin);\
    float u = (float(splash_i / atlas_y) + frag_tex_c.s) / float(atlas_x);\
    float v = (float(splash_i - (splash_i / atlas_y) * atlas_y) + frag_tex_c.t) / float(atlas_y);\
    frag_color = texture(tex, vec2(u, v));\
    vec4 mix = vec4(1.0, 1.05, 1.13, 1);\
    vec4 noise = texture(noise_tex, splash_ind_fin / float(atlas_x * atlas_y));\
    float src_a = frag_color.a;\
    if (frag_color.a == 0) {\
        noise = vec4(0.0);\
        mix = vec4(1.0);\
    }\
    \
    frag_color = mix * texture(background_tex, vec2(float(gl_FragCoord.x) / w_width + noise.s - .5,\
                                                    float(gl_FragCoord.y) / w_height + noise.t - .5));\
    frag_color.a = src_a;\
  }";

#pragma mark - Util classes

class Shader {
public: // methods
  Shader(const char* shader_text, GLenum shader_type) {
    m_shader_id = glCreateShader(shader_type);
    
    if (m_shader_id == 0) {
      throw std::runtime_error("Error creating shader type " + std::to_string(shader_type));
    }
    
    GLint len = (GLint)strlen(shader_text);
    glShaderSource(m_shader_id, 1, &shader_text, &len);
    glCompileShader(m_shader_id);
    GLint success;
    glGetShaderiv(m_shader_id, GL_COMPILE_STATUS, &success);
    if (!success) {
      GLchar log[1024];
      glGetShaderInfoLog(m_shader_id, 1024, NULL, log);
      std::cerr << log << std::endl;
      glDeleteShader(m_shader_id);
      throw std::runtime_error("Error compiling shader type " + std::to_string(shader_type));
    }
  }
  
  ~Shader() { glDeleteShader(m_shader_id); }
  
  inline GLuint descr() const { return m_shader_id; }
  
private: // methods
  Shader(const Shader &);
  Shader & operator=(const Shader &);
private: // fields
  GLuint m_shader_id;
};

class Program {
public: // methods
  Program(const char * vtx_sh, const char * frag_sh) {
    m_prog_id = glCreateProgram();
    if (m_prog_id == 0) { throw std::runtime_error("Error creating shader program\n"); }

    Shader vtx(vtx_sh, GL_VERTEX_SHADER);
    Shader frg(frag_sh, GL_FRAGMENT_SHADER);

    glAttachShader(m_prog_id, vtx.descr());
    glAttachShader(m_prog_id, frg.descr());
    
    glLinkProgram(m_prog_id);
    checkProgramErrors(m_prog_id, GL_LINK_STATUS);
  }
  
  Program(const char * vtx_sh) {
    m_prog_id = glCreateProgram();
    if (m_prog_id == 0) { throw std::runtime_error("Error creating shader program\n"); }
    
    Shader vtx(vtx_sh, GL_VERTEX_SHADER);
    glAttachShader(m_prog_id, vtx.descr());
    
    const GLchar* varyings[5] = { "init_vel_out", "splash_ind_out",
                                  "position_out", "velocity_out", "time_shift_out"};
    glTransformFeedbackVaryings(m_prog_id, 5, varyings, GL_INTERLEAVED_ATTRIBS);
  
    glLinkProgram(m_prog_id);
    checkProgramErrors(m_prog_id, GL_LINK_STATUS);
  }
  
  Program(const char * vtx_sh, const char * gmt_sh, const char * frag_sh) {
    m_prog_id = glCreateProgram();
    if (m_prog_id == 0) { throw std::runtime_error("Error creating shader program\n"); }
    
    Shader vtx(vtx_sh, GL_VERTEX_SHADER);
    Shader gmt(gmt_sh, GL_GEOMETRY_SHADER);
    Shader frg(frag_sh, GL_FRAGMENT_SHADER);
    
    glAttachShader(m_prog_id, vtx.descr());
    glAttachShader(m_prog_id, gmt.descr());
    glAttachShader(m_prog_id, frg.descr());
    
    glLinkProgram(m_prog_id);
    checkProgramErrors(m_prog_id, GL_LINK_STATUS);
  }

  
  inline void use() { glUseProgram(m_prog_id); }
  inline void stopUsing() { glUseProgram(0); }
  
  ~Program() {
    if (m_prog_id != 0) { glDeleteProgram(m_prog_id); }
  }
  
  inline GLuint desc() { return m_prog_id; }
  
  
  GLint attLc(const GLchar* attribute_name) const {
    GLint attrib_loc = glGetAttribLocation(m_prog_id, attribute_name);
    if(attrib_loc == -1) {
      throw std::runtime_error(std::string("Program attribute not found: ") +
                               attribute_name);
    }
    return attrib_loc;
  }
  
  GLint uniLc(const GLchar* uniform_name) const {
    GLint uniform_loc = glGetUniformLocation(m_prog_id, uniform_name);
    if(uniform_loc == -1) {
      throw std::runtime_error(std::string("Program uniform not found: ") +
                               uniform_name);
    }
    return uniform_loc;
  }
  
private: // methods
  void checkProgramErrors(GLint shaderProgram, GLenum checkParam) {
    GLint success = 0;
    glGetProgramiv(shaderProgram, checkParam, &success);
    if (!success) {
      GLchar log[1024];
      glGetProgramInfoLog(shaderProgram, sizeof(log), NULL, log);
      std::cerr << log << std::endl;
      
      glDeleteProgram(m_prog_id);
      throw std::runtime_error("Program check error");
    }
  }
  
private: // field
  GLuint m_prog_id;
};


#endif
