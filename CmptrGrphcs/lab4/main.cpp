//
//  main.cpp
//
//  Created by Hatless Fox on 9/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include <iostream>
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#ifdef XCODE_BLD
  #include <ImageMagick-6/Magick++.h>
#else
  #include <ImageMagick/Magick++.h>
#endif
#include <algorithm>
#include <float.h>
#include <map>
#include <vector>
#include <stdexcept>
#include <thread>

#include "Shaders.h"

float fov = 20.0;
float ah = 0;
float av = 0;
float aspectRatio;

float width = 800;
float height = 600;

float far = 20.0;
float near = .1;


struct ModeData {
  std::shared_ptr<Program> sh_prog;
  
  GLuint tex_obj;
  
  GLuint vao;
  GLuint vbo;
  GLuint ibo;
  GLint elems_cnt;
};
ModeData background;

struct ParticleData {
  GLuint vao_draw;
  GLuint vao_update;
  GLuint vbo;
  
  GLint elems_cnt;
  
  GLuint screen_texture;
  GLuint noise_texutre;
  //GLuint frame_buffer;
  //GLuint depth_buffer;
};

GLuint splash_atlas_obj;
std::shared_ptr<Program> render_prog;
std::shared_ptr<Program> update_prog;

ParticleData particlesInit;
ParticleData particlesNew;
bool buffer_flip_flag = false;

bool paused = false;
float time_mult = 1.3;
float size_mult = .9;

float particles_time = 0;
GLuint framebuffer_id;
GLuint depthbuffer_id;

void logOpenGLErrorIfExists();
void setupScreenTextureBuffer();

#pragma mark - GLUT Input handlers

void onKeyboard(GLFWwindow* window, int key, int scancode, int action, int mods) {
  if (action != GLFW_PRESS) {
    return;
  }
  
  switch (key) {
    case GLFW_KEY_W: fov -= .1; break;
    case GLFW_KEY_S: fov += .1; break;
    case GLFW_KEY_U: far += 1; break;
    case GLFW_KEY_E: far -= 1; break;
    case GLFW_KEY_H: near -= 1; break;
    case GLFW_KEY_T: near += 1; break;
    
    case GLFW_KEY_SPACE: paused = !paused; break;
    case GLFW_KEY_LEFT:  time_mult -= .1; break;
    case GLFW_KEY_RIGHT: time_mult += .1; break;
    case GLFW_KEY_UP:    size_mult += .05; break;
    case GLFW_KEY_DOWN:  size_mult -= .05; break;
  }
}

void onMotion(GLFWwindow* window, double x, double y) {
  if (glfwGetMouseButton (window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE) { return; }
  
  static double mX = 0;
  static double mY = 0;
  
  if (abs(mX - x) > 0) { ah += .5 * (mX - x > 0 ? -1 : 1); }
  if (abs(mY - y) > 0) { av += .5 * (mY - y > 0 ? 1 : -1); }
  
  mX = x;
  mY = y;
}

void onResize(GLFWwindow *, int w, int h) {
  width = w;
  height = h;
  aspectRatio = w * 1.0 / h;
  glViewport(0, 0, w, h);
  
  glDeleteTextures(1, &particlesInit.screen_texture);
  glDeleteFramebuffers(1, &framebuffer_id);
  glDeleteRenderbuffers(1, &depthbuffer_id);
  
  setupScreenTextureBuffer();
  
  particlesNew.screen_texture = particlesInit.screen_texture;
  particlesNew.noise_texutre  = particlesInit.noise_texutre;
}

#pragma mark - GL initializers

void initGlState(){
  glClearColor(0.2, 0.2, 0.2, 1.0);

  glEnable(GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

GLuint loadTexture(std::string const & file_name) {
  std::shared_ptr<Magick::Image> img(new Magick::Image(file_name));
  Magick::Blob blob;
  img->flip();
  img->write(&blob, "RGBA");

  GLuint tex_obj;
  glGenTextures(1, &tex_obj);
  glBindTexture(GL_TEXTURE_2D, tex_obj);
  
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)img->columns(), (GLsizei)img->rows(),
               0, GL_RGBA, GL_UNSIGNED_BYTE, blob.data());
  glGenerateMipmap(GL_TEXTURE_2D);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  
  return tex_obj;
}

void initBackground() {
  GLfloat plane_vtx_data[4*5] = {
    //x,  y, z, s, t
    -2.0, -2.0, -4.0, 0, 0,
    -2.0, 2.0, -4.0, 0, 1,
    2.0, 2.0, -4.0, 1, 1,
    2.0, -2.0, -4.0, 1, 0
  };
  
  glGenVertexArrays(1, &background.vao);
  glBindVertexArray(background.vao);
  
  // init data buffer
  glGenBuffers(1, &background.vbo);
  glBindBuffer(GL_ARRAY_BUFFER, background.vbo);
  glBufferData(GL_ARRAY_BUFFER,sizeof(GLfloat) * 4 * 5, plane_vtx_data, GL_STATIC_DRAW);
  
  // init elements buffer
  GLint plane_index_data[6] = {
    0, 1, 2,
    0, 2, 3
  };
  glGenBuffers(1, &background.ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, background.ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLint) * 6, plane_index_data, GL_STATIC_DRAW);
  
  background.elems_cnt = 6;
  background.sh_prog.reset(new Program(bg_vsp, bg_fsp));
  background.tex_obj = loadTexture("./lenna_head.jpg");
  
  // enable attributes
  glEnableVertexAttribArray(background.sh_prog->attLc("position"));
  glEnableVertexAttribArray(background.sh_prog->attLc("tex_coord"));
  GLsizei stride = 5 * sizeof(GLfloat);
  
  glVertexAttribPointer(background.sh_prog->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, 0);
  glVertexAttribPointer(background.sh_prog->attLc("tex_coord"), 2, GL_FLOAT, GL_FALSE, stride, (void *)(3 * sizeof(GLfloat)));
  glBindVertexArray(0);
}

void setupScreenTextureBuffer() {
  // create background texture
  glGenTextures(1, &particlesInit.screen_texture);
 
  glBindTexture(GL_TEXTURE_2D, particlesInit.screen_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
  
  glGenFramebuffers(1, &framebuffer_id);
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id);
  
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, particlesInit.screen_texture, 0);
  
  glGenRenderbuffers(1, &depthbuffer_id);
  glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer_id);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthbuffer_id);
  
  GLenum draw_buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, draw_buffers);
  
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
    assert(false);
  }
  
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
}


float calcVectorComponent(int delta, float init = 0) { return (rand() % delta)/1000.0 + init; }

void initParticles(ParticleData &data) {
  //init_spd; splash_ind; pos;
  const size_t PARTICLES_NUMBER = 300;
  
  GLsizei stride = 11 * sizeof(GLfloat);
  data.elems_cnt = PARTICLES_NUMBER;
  std::vector<GLfloat> particles_data;
  float x = 0, y = 0, z = 0;
  for (size_t i = 0; i < PARTICLES_NUMBER; ++i) {
    // setup initial speed
    x = calcVectorComponent(150) * ((rand() % 2) ? -1 : 1);
    y = calcVectorComponent(300, 0.2) + (rand() % 2)/10.0;
    z = calcVectorComponent(150) * ((rand() % 2) ? -1 : 1);
    
    particles_data.push_back(x);
    particles_data.push_back(y);
    particles_data.push_back(z);
    
    // setup splash id
    particles_data.push_back(rand() % 16);
    
    // setup pos
    particles_data.push_back(0);
    particles_data.push_back(0);
    particles_data.push_back(0);
    
    // setup velocity
    particles_data.push_back(x);
    particles_data.push_back(y);
    particles_data.push_back(z);
    
    // time shift
    particles_data.push_back(rand() % 70);
  }
  
  std::vector<GLubyte> noise_data;
  for (int i = 0; i < 16; ++i) {
    noise_data.push_back(125 + rand() % 5);
    noise_data.push_back(125 + rand() % 5);
    noise_data.push_back(0);
  }
  
  glGenTextures(1, &particlesInit.noise_texutre);
  glBindTexture(GL_TEXTURE_1D, particlesInit.noise_texutre);
  glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, (GLsizei)16,
               0, GL_RGB, GL_UNSIGNED_BYTE, &noise_data[0]);
  glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  

  
  //** generate all stuff
  // VAOs
  glGenVertexArrays(1, &data.vao_update);
  glGenVertexArrays(1, &data.vao_draw);
  // VBOs
  glGenBuffers(1, &data.vbo);
  
  /** setup upd vao */
  glBindVertexArray(data.vao_update);
  glBindBuffer(GL_ARRAY_BUFFER, data.vbo);
  glBufferData(GL_ARRAY_BUFFER, stride * data.elems_cnt, &particles_data[0], GL_DYNAMIC_DRAW);

  glEnableVertexAttribArray(update_prog->attLc("init_vel"));
  glEnableVertexAttribArray(update_prog->attLc("splash_ind"));
  glEnableVertexAttribArray(update_prog->attLc("position"));
  glEnableVertexAttribArray(update_prog->attLc("velocity"));
  glEnableVertexAttribArray(update_prog->attLc("time_shift"));
  
  glVertexAttribPointer(update_prog->attLc("init_vel"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(0 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("splash_ind"), 1, GL_FLOAT, GL_FALSE, stride, (void *)(3 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(4 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("velocity"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(7 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("time_shift"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(10 * sizeof(GLfloat)));

  //** Setup render vao
  glBindVertexArray(data.vao_draw);
  glBindBuffer(GL_ARRAY_BUFFER, data.vbo);

  glEnableVertexAttribArray(update_prog->attLc("init_vel"));
  glEnableVertexAttribArray(render_prog->attLc("splash_ind"));
  glEnableVertexAttribArray(render_prog->attLc("position"));
  glEnableVertexAttribArray(update_prog->attLc("velocity"));
  glEnableVertexAttribArray(update_prog->attLc("time_shift"));

  glVertexAttribPointer(update_prog->attLc("init_vel"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(0 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("splash_ind"), 1, GL_FLOAT, GL_FALSE, stride, (void *)(3 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("position"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(4 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("velocity"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(7 * sizeof(GLfloat)));
  glVertexAttribPointer(update_prog->attLc("time_shift"), 3, GL_FLOAT, GL_FALSE, stride, (void *)(10 * sizeof(GLfloat)));
  
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);
  
  setupScreenTextureBuffer();
  
}

GLuint transform_feedback;
void initDisplayData() {
  aspectRatio = width / height;
  
  initBackground();
  
  glGenTransformFeedbacks(1, &transform_feedback);
  glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transform_feedback);
  
  
  render_prog.reset(new Program(vsp, gsp, fsp));
  update_prog.reset(new Program(upd_vsp));
  splash_atlas_obj = loadTexture("./water_particles.png");
  
  initParticles(particlesInit);
  initParticles(particlesNew);
  
  particlesNew.screen_texture = particlesInit.screen_texture;
  particlesNew.noise_texutre  = particlesInit.noise_texutre;
  
  glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
}

#pragma mark - Renderer

void renderBackground(glm::mat4 const & proj, glm::mat4 const & cam) {
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  
  ModeData &d = background;

  //** render background to texture
  glBindFramebuffer(GL_FRAMEBUFFER, framebuffer_id);
  glBindRenderbuffer(GL_RENDERBUFFER, depthbuffer_id);
  
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  d.sh_prog->use();
  glUniform1i(d.sh_prog->uniLc("tex"), 0);
  glUniformMatrix4fv(d.sh_prog->uniLc("worldMatrix"), 1.0f, GL_FALSE, glm::value_ptr(proj * cam));
  glDrawElements(GL_TRIANGLES, d.elems_cnt, GL_UNSIGNED_INT, 0);
  
  d.sh_prog->stopUsing();
  
  //** copy rendered background to screen
  glBindFramebuffer(GL_READ_FRAMEBUFFER, framebuffer_id);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
  
  //** finalize
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, particlesInit.screen_texture);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glBindRenderbuffer(GL_RENDERBUFFER, 0);
  
  d.sh_prog->stopUsing();
}


void updateParticles() {
  glEnable(GL_RASTERIZER_DISCARD);
  glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transform_feedback);
  
  update_prog->use();
  glUniform1f(update_prog->uniLc("time_mult"), time_mult);

  if (!buffer_flip_flag) {
    glBindBuffer(GL_ARRAY_BUFFER, particlesInit.vbo);
    glBindVertexArray(particlesInit.vao_update);
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particlesNew.vbo);
  } else {
    glBindBuffer(GL_ARRAY_BUFFER, particlesNew.vbo);
    glBindVertexArray(particlesNew.vao_update);
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particlesInit.vbo);
  }
  
  glBeginTransformFeedback(GL_POINTS);
  glDrawArrays(GL_POINTS, 0, particlesInit.elems_cnt);
  glEndTransformFeedback();
  
  /*
  glBindBuffer(GL_ARRAY_BUFFER, !buffer_flip_flag ? particlesNew.vbo : particlesInit.vbo);
  glFlush();
  GLfloat feedback[5];
  glGetBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(feedback), feedback);
  printf("AFTER DST: ind - %f;\n", feedback[3]);
  */
  
  glBindVertexArray(0);
  update_prog->stopUsing();
  
  glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
  glDisable(GL_RASTERIZER_DISCARD);
  
  buffer_flip_flag = !buffer_flip_flag;
}


void renderParticles(glm::mat4 const & proj, glm::mat4 const & cam) {
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  render_prog->use();
  
  glUniform1i(render_prog->uniLc("tex"), 0);
  glUniform1i(render_prog->uniLc("background_tex"), 1);
  glUniform1i(render_prog->uniLc("noise_tex"), 2);
  glUniform1i(render_prog->uniLc("atlas_x"), 4);
  glUniform1i(render_prog->uniLc("atlas_y"), 4);
  glUniform1f(render_prog->uniLc("w_width"), width);
  glUniform1f(render_prog->uniLc("w_height"), height);
  glUniform1f(render_prog->uniLc("splash_scale"), size_mult);

  glm::mat4 model_m;
  model_m = glm::translate(model_m, glm::vec3(0, -0.5, 0));
  
  glUniformMatrix4fv(render_prog->uniLc("model_m"), 1.0f, GL_FALSE, glm::value_ptr(model_m));
  glUniformMatrix4fv(render_prog->uniLc("view_m"), 1.0f, GL_FALSE, glm::value_ptr(cam));
  glUniformMatrix4fv(render_prog->uniLc("proj_m"), 1.0f, GL_FALSE, glm::value_ptr(proj));
  
  if (buffer_flip_flag) {
    glBindVertexArray(particlesInit.vao_draw);
  } else {
    glBindVertexArray(particlesNew.vao_draw);
  }
    
  glDrawArrays(GL_POINTS, 0, particlesInit.elems_cnt);
  glBindVertexArray(0);
  render_prog->stopUsing();
}

void renderLogic(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::mat4 proj = glm::perspective(fov, aspectRatio, near, far);
  glm::mat4 camera =
    glm::rotate(
      glm::rotate(
        glm::lookAt(glm::vec3(0.0f, 0.0f, 10.0f), glm::vec3(0),
                    glm::vec3(0, 1, 0)
        ),
        ah, glm::vec3(0, 1, 0)
      ),
      av, glm::vec3(0, 0, 1)
    );

  //render BU
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, background.tex_obj);
  glBindBuffer(GL_ARRAY_BUFFER, background.vbo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, background.ibo);
  glBindVertexArray(background.vao);
  
  renderBackground(proj, camera);
  glBindVertexArray(0);
  //process particles
  
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, splash_atlas_obj);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, particlesInit.screen_texture);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_1D, particlesInit.noise_texutre);
  logOpenGLErrorIfExists();

  //TODO: Investigate: swapping following lines causes update to stall,
  //                   maybe it's binding problem or some OGL spec behavior
  renderParticles(proj, camera);
  
  if (!paused) {
    updateParticles();
    particles_time += time_mult / 25;
  }
  
  glBindVertexArray(0);
}

#pragma mark - Main and utils

void logOpenGLErrorIfExists() {
  GLenum error = glGetError();
  if(error != GL_NO_ERROR)
  std::cerr << "OpenGL Error " << error << ": " << (const char*)gluErrorString(error) << std::endl;
}

int main(int argc, const char * argv[]) {
  // initialise GLFW
  if(!glfwInit()) { throw std::runtime_error("glfwInit failed"); }
  
  // open a window with GLFW
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
  
  GLFWwindow* window = glfwCreateWindow(width, height, "Lab 4", NULL, NULL);
  glfwMakeContextCurrent(window);
  
  glewExperimental = GL_TRUE;
  if(glewInit() != GLEW_OK) { throw std::runtime_error("glewInit failed"); }
  // GLEW throws some errors, so discard all the errors so far
  while(glGetError() != GL_NO_ERROR) {}
  
  glfwSetKeyCallback(window, onKeyboard);
  glfwSetCursorPosCallback(window, onMotion);
  glfwSetWindowSizeCallback(window, onResize);
  
  initGlState();
  initDisplayData();

  while (!glfwWindowShouldClose(window)) {
    renderLogic();
    
    // check for errors
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
      std::cerr << "OpenGL Error " << error << ": " << (const char*)gluErrorString(error) << std::endl;
    
    glfwSwapBuffers(window);
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
    glfwPollEvents();
  }
  
  // clean up and exit
  glfwDestroyWindow(window);
  glfwTerminate();
  
  return 0;
}
