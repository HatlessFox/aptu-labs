//
//  main.cpp
//
//  Created by Hatless Fox on 9/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <memory>
#include <ImageMagick/Magick++.h>
#include <stdexcept>
#include <algorithm>
#include <float.h>
#include <map>
#include <vector>
#include <cassert>

#include "Shaders.h"

const int SPHERE_MODE = 2;

float fov = 20.0;
float ah = 0;
float av = 0;
float aspectRatio;
float tex_mult = 1;

float far = 20.0;
float near = .1;


struct ModeData {
  std::shared_ptr<Program> sh_prog;
  GLfloat w;
  GLfloat h;

  GLuint tex_obj;
  GLuint man_tex_mip_obj;
  GLuint vbo;
  GLuint ibo;
  GLint elems_cnt;
};

std::shared_ptr<Program> mesh_prog;

ModeData modes[3];
unsigned curr_mode;
unsigned mip_display;
unsigned man_mip_tex_mode;

#pragma mark - GLUT Input handlers

void onKeyboard(unsigned char key, int, int) {
  switch (key) {
    case 'w': case 'W': fov -= 1; break;
    case 's': case 'S': fov += 1; break;
    case 'u': case 'U': far += 1; break;
    case 'e': case 'E': far -= 1; break;
    case 'h': case 'H': near += 1; break;
    case 't': case 'T': near -= 1; break;

    case 'j': case 'J': tex_mult -= .1; break;
    case 'k': case 'K': tex_mult += .1; break;
  }
  glutPostRedisplay();
}

void onMotion(int x, int y) {
  static int mX = 0;
  static int mY = 0;

  if (abs(mX - x) > 0) { ah += .5 * (mX - x > 0 ? -1 : 1); }
  if (abs(mY - y) > 0) { av += .5 * (mY - y > 0 ? 1 : -1); }

  mX = x;
  mY = y;

  glutPostRedisplay();
}

void onReshape(int w, int h) {
  aspectRatio = w * 1.0 / h;
  glViewport(0, 0, w, h);
}

void handleModeSelection(int num) {
  curr_mode = num;
  if (man_mip_tex_mode && curr_mode == 2) {
    assert(0 && "Custom MIP mapiing is supported only for plane and cube");
  }
  glutPostRedisplay();
}

void handleMainMenuSelection(int num) {
  if (num == 0) {
    mip_display = !mip_display;
  } else if (num == 1) {
    man_mip_tex_mode = !man_mip_tex_mode;
    if (man_mip_tex_mode && curr_mode == 2) {
      assert(0 && "Custom MIP mapiing is supported only for plane and cube");
    }
  }
  glutPostRedisplay();
}


void handleFiltrationSelection(int num) {
  GLenum filt = GL_LINEAR;
  switch (num) {
    case 0: {
      filt = GL_LINEAR;
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filt);
      break;
    }
    case 1: {
      filt = GL_NEAREST;
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filt);
      break;
    }
    case 2: filt = GL_NEAREST_MIPMAP_NEAREST; break;
    case 3: filt = GL_NEAREST_MIPMAP_LINEAR; break;
    case 4: filt = GL_LINEAR_MIPMAP_NEAREST; break;
    case 5: filt = GL_LINEAR_MIPMAP_LINEAR; break;
    default:
      break;
  }

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filt);
  glutPostRedisplay();
}

#pragma mark - GL initializers

void initGlState(){
  glClearColor(0.2, 0.2, 0.2, 1.0);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
}

GLuint loadTexture(std::string const & file_name, ModeData *md) {
  std::shared_ptr<Magick::Image> img(new Magick::Image(file_name));
  Magick::Blob blob;
  if (img->columns() < 256) { // lena head case
    img->resize(Magick::Geometry(256, 256));
  }
  img->flip();
  img->write(&blob, "RGBA");

  GLuint tex_obj;
  glGenTextures(1, &tex_obj);
  glBindTexture(GL_TEXTURE_2D, tex_obj);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8,
               (GLsizei)img->columns(), (GLsizei)img->rows(),
               0, GL_RGBA, GL_UNSIGNED_BYTE, blob.data());
  glGenerateMipmap(GL_TEXTURE_2D);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  md->w = img->columns();
  md->h = img->rows();

  //gen tex for man min tex mode
  GLuint mm_tex_obj;
  glGenTextures(1, &mm_tex_obj);
  glBindTexture(GL_TEXTURE_2D, mm_tex_obj);
  glActiveTexture(GL_TEXTURE0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 5);
  for (int lvl = 0; lvl < 6; ++lvl) {
    Magick::Blob blob;
    img->resize(Magick::Geometry(1 << (8 - lvl), 1 << (8 - lvl)));
    img->write(&blob, "RGBA");
    glTexImage2D(GL_TEXTURE_2D, lvl, GL_RGB8,
                 (GLsizei)img->columns(), (GLsizei)img->rows(),
                 0, GL_RGBA, GL_UNSIGNED_BYTE, blob.data());
    img->rotate(90);
  }
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_NEAREST_MIPMAP_NEAREST);
  md->man_tex_mip_obj = mm_tex_obj;

  return tex_obj;
}

void initPlaneMode() {
  GLfloat plane_vtx_data[4*5] = {
    //x,  y, z, s, t
    -1.0, -1.0, 0.0, 0, 0,
    -1.0, 1.0, 0.0, 0, 1,
    1.0, 1.0, 0.0, 1, 1,
    1.0, -1.0, 0.0, 1, 0
  };

  glGenBuffers(1, &(modes[0].vbo));
  glBindBuffer(GL_ARRAY_BUFFER, modes[0].vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 5 * 4,
               plane_vtx_data, GL_STATIC_DRAW);

  GLint plane_index_data[6] = {
    0, 1, 2,
    0, 2, 3
  };
  glGenBuffers(1, &(modes[0].ibo));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, modes[0].ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLint) * 6,
               plane_index_data, GL_STATIC_DRAW);

  modes[0].elems_cnt = 6;
  modes[0].sh_prog.reset(new Program(vsp, fsp));
  modes[0].tex_obj = loadTexture("./lenna_head.jpg", modes);
}

void initCubeMode() {
  GLfloat plane_vtx_data[4*5*6] = {
    //x,  y, z, s, t
    //front face
    -1.0, -1.0, 1.0, 0, 0,
    -1.0,  1.0, 1.0, 0, 1,
    1.0,   1.0, 1.0, 1, 1,
    1.0,  -1.0, 1.0, 1, 0,

    //left face
    -1.0, -1.0,  1.0, 1, 0,
    -1.0,  1.0,  1.0, 1, 1,
    -1.0,  1.0, -1.0, 0, 1,
    -1.0, -1.0, -1.0, 0, 0,

    //right face
    1.0, -1.0,  1.0, 0, 0,
    1.0,  1.0,  1.0, 0, 1,
    1.0,  1.0, -1.0, 1, 1,
    1.0, -1.0, -1.0, 1, 0,

    //back face
    -1.0, -1.0, -1.0, 1, 0,
    -1.0,  1.0, -1.0, 1, 1,
    1.0,   1.0, -1.0, 0, 1,
    1.0,  -1.0, -1.0, 0, 0,

    //top face
    -1.0, 1.0, -1.0, 0, 0,
    -1.0, 1.0,  1.0, 0, 1,
     1.0, 1.0,  1.0, 1, 1,
     1.0, 1.0, -1.0, 1, 0,

    //bottom face
    -1.0, -1.0, -1.0, 0, 0,
    -1.0, -1.0,  1.0, 0, 1,
     1.0, -1.0,  1.0, 1, 1,
     1.0, -1.0, -1.0, 1, 0,
  };

  glGenBuffers(1, &(modes[1].vbo));
  glBindBuffer(GL_ARRAY_BUFFER, modes[1].vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 5 * 4 * 6,
               plane_vtx_data, GL_STATIC_DRAW);

  GLint plane_index_data[6 * 6] = {
    0, 1, 2,
    0, 2, 3,
    4, 5, 6,
    4, 6, 7,
    8, 9, 10,
    8, 10, 11,
    12, 13, 14,
    12, 14, 15,
    16, 17, 18,
    16, 18, 19,
    20, 21, 22,
    20, 22, 23
  };
  glGenBuffers(1, &(modes[1].ibo));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, modes[1].ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLint) * 6 * 6,
               plane_index_data, GL_STATIC_DRAW);

  modes[1].elems_cnt = 6 * 6;
  modes[1].sh_prog.reset(new Program(vsp, fsp));

  modes[1].tex_obj = modes[0].tex_obj;
  modes[1].man_tex_mip_obj = modes[0].man_tex_mip_obj;
  modes[1].w = modes[0].w;
  modes[1].h = modes[0].h;
}

void pushPlaneCoord(std::vector<float> &st, std::vector<float> *dest) {
  for (int i = 0; i < 4; ++i) {
    for (size_t j = 0; j < st.size(); ++j) {
      if (st[j] == 0) { dest->push_back(st[j]); }
      else {
        int bit = fabs(fabs(st[j]) - 1) < FLT_EPSILON ? 0 : 1;
        dest->push_back(st[j] * ((i & 1 << bit) ? -1 : 1));
      }
    }
  }
}

int genMiddle(size_t p1i, size_t p2i, std::vector<float> *vtxs,
              std::map<int64_t, int> *cache) {
  int64_t key = ((uint64_t)p1i << 32) + p2i; //traversal always clockwise
  if (cache->count(key)) { return cache->at(key); }

  size_t new_ind = vtxs->size() / 3;
  cache->operator[](key) = (int)new_ind;

  float x = (vtxs->at(p1i * 3) + vtxs->at(p2i * 3)) / 2;
  float y = (vtxs->at(p1i * 3 + 1) + vtxs->at(p2i * 3 + 1)) / 2;
  float z = (vtxs->at(p1i * 3 + 2) + vtxs->at(p2i * 3 + 2)) / 2;
  float len = sqrtf(x*x + y*y + z*z);
  vtxs->push_back(x / len);
  vtxs->push_back(y / len);
  vtxs->push_back(z / len);

  return (int)new_ind;
}

void generateAndInitSphere(ModeData *mode) {
  std::vector<float> vtxs;
  std::vector<int> inds;

  //create base icosahedron
  float gr = (1 + sqrt(5)) / 2;
  std::vector<float> base = { -1, gr, 0 };
  for (int i = 0; i < 3; i++) {
    pushPlaneCoord(base, &vtxs);
    std::rotate(base.begin(), base.begin() + 2, base.end());
  }

  //normalize vtxes
  for (int p_ind = 0; p_ind < vtxs.size(); p_ind += 3) {
    float len = sqrtf(vtxs[p_ind] * vtxs[p_ind] +
                      vtxs[p_ind + 1] * vtxs[p_ind + 1] +
                      vtxs[p_ind + 2] * vtxs[p_ind + 2]);
    vtxs[p_ind] /= len;
    vtxs[p_ind + 1] /= len;
    vtxs[p_ind + 2] /= len;
  }

  inds = { 0, 11, 5,/**/0,  5,  1,/**/ 0,  1,  7,/**/ 0, 7, 10,/**/0, 10, 11,/**/
           1,  5, 9,/**/5, 11,  4,/**/11, 10,  2,/**/10, 7,  6,/**/7,  1,  8,/**/
           3,  9, 4,/**/3,  4,  2,/**/ 3,  2,  6,/**/ 3, 6,  8,/**/3,  8,  9,/**/
           4,  9, 5,/**/2,  4, 11,/**/ 6,  2, 10,/**/ 8, 6,  7,/**/9,  8, 1};

  //enhanse icosahedron to be more like sphere
  for (int ench_iter = 0; ench_iter < 3; ++ench_iter) {
    std::vector<int> tmp_inds;
    std::map<int64_t, int> mid_cache;

    for (int face_ind = 0; face_ind < inds.size(); face_ind += 3) {
      int p1 = inds[face_ind];
      int p2 = inds[face_ind + 1];
      int p3 = inds[face_ind + 2];

      int mid01 = genMiddle(p1, p2, &vtxs, &mid_cache);
      int mid12 = genMiddle(p2, p3, &vtxs, &mid_cache);
      int mid20 = genMiddle(p3, p1, &vtxs, &mid_cache);

      tmp_inds.push_back(p1);
      tmp_inds.push_back(mid01); tmp_inds.push_back(mid20);

      tmp_inds.push_back(p2);
      tmp_inds.push_back(mid12); tmp_inds.push_back(mid01);

      tmp_inds.push_back(p3);
      tmp_inds.push_back(mid20); tmp_inds.push_back(mid12);

      tmp_inds.push_back(mid01);
      tmp_inds.push_back(mid12); tmp_inds.push_back(mid20);
    }

    inds = tmp_inds;
  }

  glGenBuffers(1, &(mode->vbo));
  glBindBuffer(GL_ARRAY_BUFFER, mode->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * vtxs.size(),
               &vtxs[0], GL_STATIC_DRAW);

  glGenBuffers(1, &(mode->ibo));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mode->ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLint) * inds.size(),
               &inds[0], GL_STATIC_DRAW);

  mode->elems_cnt = (GLuint)inds.size();
}

void initSphereMode() {
  generateAndInitSphere(modes + 2);

  modes[2].sh_prog.reset(new Program(sph_vs, sph_fs));
  modes[2].tex_obj = loadTexture("./earth_texture_grid.bmp", (modes + 2));
  mesh_prog.reset(new Program(mesh_vsp, mesh_fsp));
}

void initDisplayData() {
  aspectRatio = 640.0 / 480;

  initPlaneMode();
  initCubeMode();
  initSphereMode();

  curr_mode = 0;
  mip_display = 0;
  man_mip_tex_mode = 0;
}

#pragma mark - Renderer

void renderMesh(glm::mat4 const & proj, glm::mat4 const & cam) {
  glDepthFunc(GL_LEQUAL);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  mesh_prog->use();

  glUniformMatrix4fv(mesh_prog->uniLc("worldMatrix"), 1.0f, GL_FALSE,
                     glm::value_ptr(proj * cam));

  ModeData &d = modes[curr_mode];
  glDrawElements(GL_TRIANGLES, d.elems_cnt, GL_UNSIGNED_INT, 0);

  mesh_prog->stopUsing();

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glDepthFunc(GL_LESS);
}

void renderModel(glm::mat4 const & proj, glm::mat4 const & cam) {
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  ModeData &d = modes[curr_mode];

  d.sh_prog->use();
  glUniform1i(d.sh_prog->uniLc("tex"), 0);
  glUniform1i(d.sh_prog->uniLc("mip_display"), mip_display);
  glUniform1f(d.sh_prog->uniLc("img_width"), d.w);
  glUniform1f(d.sh_prog->uniLc("img_height"), d.h);

  glUniform1f(d.sh_prog->uniLc("tex_m"), tex_mult);
  glUniformMatrix4fv(d.sh_prog->uniLc("worldMatrix"), 1.0f,
                     GL_FALSE, glm::value_ptr(proj * cam));
  glDrawElements(GL_TRIANGLES, d.elems_cnt, GL_UNSIGNED_INT, 0);

  d.sh_prog->stopUsing();

  if (curr_mode == 2) {
    renderMesh(proj, cam);
  }
}

void renderLogic(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::mat4 proj = glm::perspective(fov, aspectRatio, near, far);
  glm::mat4 camera =
    glm::rotate(
      glm::rotate(
        glm::lookAt(glm::vec3(0.0f, 0.0f, 10.0f), glm::vec3(0),
                    glm::vec3(0, 1, 0)
        ),
        ah, glm::vec3(0, 1, 0)
      ),
      av, glm::vec3(0, 0, 1)
    );

  ModeData &d = modes[curr_mode];
  
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, man_mip_tex_mode ? d.man_tex_mip_obj : d.tex_obj);
  
  glBindBuffer(GL_ARRAY_BUFFER, d.vbo);
  glEnableVertexAttribArray(d.sh_prog->attLc("position"));
  glVertexAttribPointer(d.sh_prog->attLc("position"), 3, GL_FLOAT, GL_FALSE,
                        sizeof(GLfloat) * (curr_mode == SPHERE_MODE ? 0 : 5), 0);
  if (curr_mode != SPHERE_MODE) {
    glEnableVertexAttribArray(d.sh_prog->attLc("tex_coord"));
    glVertexAttribPointer(d.sh_prog->attLc("tex_coord"), 2, GL_FLOAT, GL_FALSE,
                          sizeof(GLfloat) * 5, (void *) (sizeof(GLfloat) * 3));
  }
    
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, d.ibo);
  renderModel(proj, camera);
  
  if (curr_mode != SPHERE_MODE) {
    glDisableVertexAttribArray(d.sh_prog->attLc("tex_coord"));
  }
  glDisableVertexAttribArray(d.sh_prog->attLc("position"));
  glutSwapBuffers();
}

#pragma mark - Main and utils

void createMenu() {
  int mode_submenu = glutCreateMenu(handleModeSelection);
  glutAddMenuEntry("Plane", 0);
  glutAddMenuEntry("Cube", 1);
  glutAddMenuEntry("Sphere", 2);

  int filter_submenu = glutCreateMenu(handleFiltrationSelection);
  glutAddMenuEntry("Linear", 0);
  glutAddMenuEntry("Nearest", 1);
  glutAddMenuEntry("Near MIP Near", 2);
  glutAddMenuEntry("Near MIP Lin", 3);
  glutAddMenuEntry("Lin MIP Near", 4);
  glutAddMenuEntry("Lin MIP Lin", 5);
  
  glutCreateMenu(handleMainMenuSelection);
  glutAddSubMenu("Mode", mode_submenu);
  glutAddSubMenu("Filtration", filter_submenu);
  glutAddMenuEntry("Toggle MIP display", 0);
  glutAddMenuEntry("Man MIP Tex Mode", 1);

  glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, const char * argv[]) {
  // init GLUT
  glutInit(&argc, (char **)argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

  // create window
  glutInitWindowSize(640, 480);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("Lab 2");
  glutKeyboardFunc(onKeyboard);
  glutMotionFunc(onMotion);
  glutReshapeFunc(onReshape);
  createMenu();

  GLenum res = glewInit();
  if (res != GLEW_OK) {
    fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
    return 1;
  }

  glutDisplayFunc(renderLogic);
  initGlState();
  initDisplayData();

  glutMainLoop();
  return 0;
}
