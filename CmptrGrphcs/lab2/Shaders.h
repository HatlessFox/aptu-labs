//
//  Shaders.h
//  test_Graphics
//
//  Created by Hatless Fox on 10/15/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef test_Graphics_Shaders_h
#define test_Graphics_Shaders_h

/**********************/

static const char* mesh_vsp = "\
  attribute vec3 position;  \
  uniform mat4 worldMatrix; \
  \
  void main() { gl_Position = worldMatrix * vec4(position, 1.0); }";

static const char* mesh_fsp = "\
  void main() { gl_FragColor = vec4(0, 0.5, 0, 1); }";


static const char* sph_vs = "\
  attribute vec3 position;  \
  uniform mat4 worldMatrix; \
  varying vec3 frag_vtx_pos;\
  \
  void main() {\
    frag_vtx_pos = position;\
    gl_Position = worldMatrix * vec4(position, 1.0);\
  }";

static const char* sph_fs = "\
  uniform float img_width;\
  uniform float img_height;\
  \
  uniform int mip_display;\
  uniform sampler2D tex;\
  uniform float tex_m;\
  varying vec3 frag_vtx_pos;\
  void main() {\
    float PI = 3.141592;\
    \
    float z = frag_vtx_pos.z;\
    float x = frag_vtx_pos.x;\
    float man_half_atan2 = atan(z, sqrt(z*z + x*x) + x);\
    \
    float s = man_half_atan2 / PI + .5;\
    float t = 1.0 - acos(frag_vtx_pos.y) / PI;\
    \
    vec4 intensity = vec4(1);\
    vec2 tc = vec2(s, t);\
    vec2 tc_scaled = vec2(s * img_width, t * img_height);\
    vec2 dx = dFdx(tc_scaled);\
    vec2 dy = dFdy(tc_scaled);\
    \
    if (mip_display == 1) {\
      float d = sqrt(max(dot(dx, dx), dot(dy, dy)));\
      int level = int(max(0.0, log2(d)) + .5);\
      if (level == 0) {}\
      else if (level == 1) { intensity = vec4(1.0, 0, 0, 1); }\
      else if (level == 2) { intensity = vec4(0.5, 0.5, 0, 1); }\
      else if (level == 3) { intensity = vec4(0, 1.0, 0, 1); }\
      else if (level == 4) { intensity = vec4(0, 0.5, 0.5, 1); }\
      else if (level == 5) { intensity = vec4(0, 0, 1.0, 1); }\
      else { intensity = vec4(1.0, 0, 1.0, 1); }\
    }\
    \
    gl_FragColor = intensity * texture2D(tex, tex_m * tc);\
  }";

/**********************/

static const char* vsp = "\
  attribute vec3 position;  \
  attribute vec2 tex_coord;  \
  \
  varying vec2 frag_tex_c;\
  uniform mat4 worldMatrix; \
  \
  void main() {\
    frag_tex_c = tex_coord;\
    gl_Position = worldMatrix * vec4(position, 1.0);\
  }";

static const char* fsp = "\
  uniform float img_width;\
  uniform float img_height;\
  \
  uniform int mip_display;\
  uniform sampler2D tex;\
  uniform float tex_m;\
  \
  varying vec2 frag_tex_c;\
  void main() {\
    vec4 intensity = vec4(1);\
    vec2 ddx = dFdx(frag_tex_c.st * img_width);\
    vec2 ddy = dFdy(frag_tex_c.st * img_height);\
    \
    vec2 ddxx = vec2(ddx.x, ddy.x);\
    vec2 ddyy = vec2(ddx.y, ddy.y);\
    \
    if (mip_display == 1) {\
      float d = sqrt(max(dot(ddxx, ddxx), dot(ddyy, ddyy)));\
      int level = int(max(0.0, log2(d)) + .40);\
      if (level == 0) {}\
      else if (level == 1) { intensity = vec4(1.0, 0, 0, 1); }\
      else if (level == 2) { intensity = vec4(0.5, 0.5, 0, 1); }\
      else if (level == 3) { intensity = vec4(0, 1.0, 0, 1); }\
      else if (level == 4) { intensity = vec4(0, 0.5, 0.5, 1); }\
      else if (level == 5) { intensity = vec4(0, 0, 1.0, 1); }\
      else { intensity = vec4(1.0, 0, 1.0, 1); }\
    }\
    gl_FragColor = intensity * texture2D(tex, tex_m * frag_tex_c.st);\
  }";

#pragma mark - Util classes

class Shader {
public: // methods
  Shader(const char* shader_text, GLenum shader_type) {
    m_shader_id = glCreateShader(shader_type);
    
    if (m_shader_id == 0) {
      throw std::runtime_error("Error creating shader type " + std::to_string(shader_type));
    }
    
    GLint len = (GLint)strlen(shader_text);
    glShaderSource(m_shader_id, 1, &shader_text, &len);
    glCompileShader(m_shader_id);
    GLint success;
    glGetShaderiv(m_shader_id, GL_COMPILE_STATUS, &success);
    if (!success) {
      GLchar log[1024];
      glGetShaderInfoLog(m_shader_id, 1024, NULL, log);
      std::cerr << log << std::endl;
      glDeleteShader(m_shader_id);
      throw std::runtime_error("Error compiling shader type " + std::to_string(shader_type));
    }
  }
  
  ~Shader() { glDeleteShader(m_shader_id); }
  
  inline GLuint descr() const { return m_shader_id; }
  
private: // methods
  Shader(const Shader &);
  Shader & operator=(const Shader &);
private: // fields
  GLuint m_shader_id;
};

class Program {
public: // methods
  Program(const char * vtx_sh, const char * frag_sh) {
    m_prog_id = glCreateProgram();
    if (m_prog_id == 0) { throw std::runtime_error("Error creating shader program\n"); }
    
    Shader vtx(vtx_sh, GL_VERTEX_SHADER);
    Shader frg(frag_sh, GL_FRAGMENT_SHADER);
    
    glAttachShader(m_prog_id, vtx.descr());
    glAttachShader(m_prog_id, frg.descr());
    
    glLinkProgram(m_prog_id);
    checkProgramErrors(m_prog_id, GL_LINK_STATUS);
  }
  
  inline void use() { glUseProgram(m_prog_id); }
  inline void stopUsing() { glUseProgram(0); }
  
  ~Program() {
    if (m_prog_id != 0) { glDeleteProgram(m_prog_id); }
  }
  
  inline GLuint desc() { return m_prog_id; }
  
  
  GLint attLc(const GLchar* attribute_name) const {
    GLint attrib_loc = glGetAttribLocation(m_prog_id, attribute_name);
    if(attrib_loc == -1) {
      throw std::runtime_error(std::string("Program attribute not found: ") +
                               attribute_name);
    }
    return attrib_loc;
  }
  
  GLint uniLc(const GLchar* uniform_name) const {
    GLint uniform_loc = glGetUniformLocation(m_prog_id, uniform_name);
    if(uniform_loc == -1) {
      throw std::runtime_error(std::string("Program uniform not found: ") +
                               uniform_name);
    }
    return uniform_loc;
  }
  
private: // methods
  void checkProgramErrors(GLint shaderProgram, GLenum checkParam) {
    GLint success = 0;
    glGetProgramiv(shaderProgram, checkParam, &success);
    if (!success) {
      GLchar log[1024];
      glGetProgramInfoLog(shaderProgram, sizeof(log), NULL, log);
      std::cerr << log << std::endl;
      
      glDeleteProgram(m_prog_id);
      throw std::runtime_error("Program check error");
    }
  }
  
private: // field
  GLuint m_prog_id;
};


#endif
