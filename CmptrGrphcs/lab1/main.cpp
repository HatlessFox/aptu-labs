//
//  main.cpp
//
//  Created by Hatless Fox on 9/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#include <iostream>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shaders.h"
#include "ObjFileLoader.h"


int mode = 0;

GLuint vbo;
GLuint ibo;

float fov = 45.0;
float ah = 0;
float av = 0;
float aspectRatio;

float far = 20.0;
float near = .1;

size_t elems_cnt;

GLuint cowShaderId;
GLuint meshShaderId;

#pragma mark - GLUT Input handlers

void onKeyboard(unsigned char key, int, int) {
  switch (key) {
    case 'w': case 'W': fov -= 1; break;
    case 's': case 'S': fov += 1; break;
    case 'u': case 'U': far += 1; break;
    case 'e': case 'E': far -= 1; break;
    case 'h': case 'H': near += 1; break;
    case 't': case 'T': near -= 1; break;
  }
  glutPostRedisplay();
}

void onMotion(int x, int y) {
  static int mX = 0;
  static int mY = 0;

  if (abs(mX - x) > 0) { ah += .5 * (mX - x > 0 ? -1 : 1); }
  if (abs(mY - y) > 0) { av += .5 * (mY - y > 0 ? 1 : -1); }

  mX = x;
  mY = y;

  glutPostRedisplay();
}

void onReshape(int w, int h) {
  aspectRatio = w * 1.0 / h;
  glViewport(0, 0, w, h);
}

void handleMenuSelection(int num) {
  mode = num;
  switch (mode) {
    case 0: cowShaderId = compileShaders(vshV1, fshV1); break;
    case 1: cowShaderId = compileShaders(vshV2, fshV2); break;
  }
  glutPostRedisplay();

}
#pragma mark - GL initializers

void initGlState(){
  glClearColor(1.0, 1.0, 1.0, 1.0);

  glEnable(GL_POLYGON_OFFSET_LINE);
  glEnable(GL_DEPTH_TEST);
}

void initDisplayData() {
  aspectRatio = 640.0 / 480;

  ObjFileLoader ldr;
  ldr.loadFromFile("./model.obj");

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, ldr.loaded_vtx_coords_size(),
               ldr.loaded_vtx_coords(), GL_STATIC_DRAW);

  glGenBuffers(1, &ibo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, ldr.loaded_faces_size(),
               ldr.loaded_faces(), GL_STATIC_DRAW);

  elems_cnt = ldr.elems_cnt();

  cowShaderId = compileShaders(vshV1, fshV1);
  meshShaderId = compileShaders(vsp, fsp);
}

#pragma mark - Renderer

void renderCow(glm::mat4 const & proj, glm::mat4 const & cam) {
  glDepthFunc(GL_LESS);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glUseProgram(cowShaderId);

  GLuint worldMatrLoc = glGetUniformLocation(cowShaderId, "worldMatrix");
  glUniformMatrix4fv(worldMatrLoc, 1.0f, GL_FALSE, glm::value_ptr(proj * cam));

  if (mode == 0) {
    GLuint camMatrLoc = glGetUniformLocation(cowShaderId,"cameraMatrix");
    glUniformMatrix4fv(camMatrLoc, 1.0f, GL_FALSE, glm::value_ptr(cam));
  }

  GLuint farLoc = glGetUniformLocation(cowShaderId,"far");
  glUniform1f(farLoc, far);
  GLuint nearLoc = glGetUniformLocation(cowShaderId,"near");
  glUniform1f(nearLoc, near);

  glDrawElements(GL_TRIANGLES, (GLint)elems_cnt, GL_UNSIGNED_INT, 0);
}

void renderMesh(glm::mat4 const & proj, glm::mat4 const & cam) {
  glDepthFunc(GL_LEQUAL);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  glUseProgram(meshShaderId);
  GLuint worldMatrLoc = glGetUniformLocation(meshShaderId,"worldMatrix");
  glUniformMatrix4fv(worldMatrLoc, 1.0f, GL_FALSE, glm::value_ptr(proj * cam));

  glDrawElements(GL_TRIANGLES, (GLint)elems_cnt, GL_UNSIGNED_INT, 0);
}

void renderLogic(void){
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glm::mat4 proj = glm::perspective(fov, aspectRatio, near, far);
  glm::mat4 camera =
    glm::rotate(
      glm::rotate(
        glm::lookAt(glm::vec3(10.0f, 0.0f, 0.0f), glm::vec3(0),
                    glm::vec3(0, 1, 0)
        ),
        ah, glm::vec3(0, 1, 0)
      ),
      av, glm::vec3(0, 0, 1)
    );

  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

  renderCow(proj, camera);

  glPolygonOffset(-0.2, -0.1);
  renderMesh(proj, camera);

  glDisableVertexAttribArray(0);
  glutSwapBuffers();
}

#pragma mark - Main and utils

void createMenu() {
  glutCreateMenu(handleMenuSelection);

  glutAddMenuEntry("Z explicit", 0);
  glutAddMenuEntry("Z implicit", 1);

  glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, const char * argv[]) {
  // init GLUT
  glutInit(&argc, (char **)argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);

  // create window
  glutInitWindowSize(640, 480);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("Lab 1");
  glutKeyboardFunc(onKeyboard);
  glutMotionFunc(onMotion);
  glutReshapeFunc(onReshape);
  createMenu();

  GLenum res = glewInit();
  if (res != GLEW_OK) {
    fprintf(stderr, "Error: '%s'\n", glewGetErrorString(res));
    return 1;
  }

  glutDisplayFunc(renderLogic);
  initGlState();
  initDisplayData();

  glutMainLoop();
  return 0;
}
