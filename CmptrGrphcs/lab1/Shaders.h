//
//  Shaders.h
//  test_Graphics
//
//  Created by Hatless Fox on 10/15/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#ifndef test_Graphics_Shaders_h
#define test_Graphics_Shaders_h

static const char* vshV1 = "\
  attribute vec3 position;                           \
  varying float camZ;                                   \
  uniform mat4 worldMatrix;                          \
  uniform mat4 cameraMatrix;                         \
  \
  void main() {                                      \
    camZ = (cameraMatrix * vec4(position, 1.0)).z;      \
    gl_Position = worldMatrix * vec4(position, 1.0); \
  }";

static const char* fshV1 = "\
  varying float camZ;\
  uniform float far;\
  uniform float near;\
  \
  void main() { \
    float clr = (-camZ - near) / (far - near); \
    gl_FragColor = vec4(clr, clr, clr, 1.0); \
  }";

/**********************/

static const char* vshV2 = "\
  attribute vec3 position;                           \
  uniform mat4 worldMatrix;                          \
  \
  void main() {                                      \
    gl_Position = worldMatrix * vec4(position, 1.0); \
  }";

static const char* fshV2 = "\
  uniform float far;\
  uniform float near;\
  \
  void main() {                                                       \
    float normalizedZ = mix(-1.0, 1.0, gl_FragCoord.z);                \
    float camZ = 2.0*far*near/(normalizedZ*(far - near)-(far + near)); \
    float clr = (-camZ - near) / (far - near);                        \
    gl_FragColor = vec4(clr, 0, clr,1.0);                             \
  }";

/**********************/

static const char* vsp = "\
  attribute vec3 position;  \
  uniform mat4 worldMatrix; \
  \
  void main() { gl_Position = worldMatrix * vec4(position, 1.0); }";

static const char* fsp = "void main() { gl_FragColor = vec4(0, 0.5, 0, 1); }";

void addShader(GLuint shaderProgram, const char* shaderText,
               GLenum shaderType) {
  GLuint shaderId = glCreateShader(shaderType);

  if (shaderId == 0) {
    fprintf(stderr, "Error creating shader type %d\n", shaderType);
    exit(0);
  }

  GLint len = (GLint)strlen(shaderText);
  glShaderSource(shaderId, 1, &shaderText, &len);
  glCompileShader(shaderId);
  GLint success;
  glGetShaderiv(shaderId, GL_COMPILE_STATUS, &success);
  if (!success) {
    GLchar log[1024];
    glGetShaderInfoLog(shaderId, 1024, NULL, log);
    fprintf(stderr, "Error compiling shader type %d: '%s'\n", shaderId, log);
    exit(1);
  }

  glAttachShader(shaderProgram, shaderId);
}


void checkProgramErrors(GLint shaderProgram, GLenum checkParam) {
  GLint success = 0;
  glGetProgramiv(shaderProgram, checkParam, &success);
  if (!success) {
    GLchar log[1024];
    glGetProgramInfoLog(shaderProgram, sizeof(log), NULL, log);
    fprintf(stderr, "Error linking shader program: '%s'\n", log);
    exit(1);
  }
}

static GLuint compileShaders(char const *vs, char const *fs) {
  GLuint shaderProgram = glCreateProgram();
  if (shaderProgram == 0) {
    fprintf(stderr, "Error creating shader program\n");
    exit(1);
  }

  addShader(shaderProgram, vs, GL_VERTEX_SHADER);
  addShader(shaderProgram, fs, GL_FRAGMENT_SHADER);

  glLinkProgram(shaderProgram);
  checkProgramErrors(shaderProgram, GL_LINK_STATUS);
  glValidateProgram(shaderProgram);
  checkProgramErrors(shaderProgram, GL_VALIDATE_STATUS);

  return shaderProgram;
}

#endif
