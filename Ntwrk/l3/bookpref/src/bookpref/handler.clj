(ns bookpref.handler
  (:use ring.adapter.jetty ring.handler.dump
        compojure.core clojure.walk ring.util.response
        [clojure.string :only [split]]
        [ring.middleware.format-response :only [wrap-restful-response]]
        bookpref.data bookpref.actions)
  (:require [compojure.handler :as handler] 
            [compojure.route :as route]))

(defn- req-for-hist [req]
  {:uri (:uri req)
   :req-type (:request-method req) 
   :params (keywordize-keys (:params req))})

(defmacro with-hist [req action] 
  `(let [tmp# (add-req (req-for-hist ~req))] ~action))

(defn mk-resp [text code] (-> (response text)(status code)))
(defmacro if-book-exists [id action]
  `(if (-> ~id Integer. find-book empty?)
    (mk-resp "Book doesn't exist" 410) ~action))
(defmacro if-book-doesnt-exist [id action]
  `(if (-> ~id Integer. find-book empty?) ~action (mk-resp "Book exists" 409)))
(defmacro if-task-exists [id action]
  `(if (-> ~id Integer. find-task empty?)
    (mk-resp "Task doesn't exist" 410) ~action))
(defmacro if-task-doesnt-exist [id action]
  `(if (-> ~id Integer. find-task empty?) ~action (mk-resp "Task exists" 409)))
(defn not-impl [] (mk-resp "Not Implemented" 501))
(defn has-value [params arg] (-> arg params empty? not))

(defn gen-dwn-summary-response [type conv]
  {:status 200
   :headers {"Content-Disposition" (str "attachment;filename=\"smr." type "\""),
             "Content-Type" (str "application/" type)}
   :body (conv (find-all-books)) })


(defn- required-view [req]
  (let [accept-hdr (-> req :headers keywordize-keys :accept)]
    (if (empty? accept-hdr) "UNKNOWN"
      (let [accpts (map #(split % #"/") (split (first (split accept-hdr #";")) #","))
            types (filter #(contains? supported-view-types %) (flatten accpts))]
        (if (empty? types) "UNKNOWN" (first types))))))

(defroutes app-routes
  (context "/book" []
    (OPTIONS "/" [] (response {:version "0.1"}))

    (GET "/" [] (mk-resp (map book-to-string (find-all-books)) 200))
    (GET ["/:id", :id #"[0-9]+"] [id]
      (let [book (-> id Integer. find-book)]
        (if (empty? book)
          (mk-resp "Book is unavailable" 404) (mk-resp book 200))))
    (DELETE ["/:id", :id #"[0-9]+"] [id]
      (if-book-exists id 
        (if (-> id Integer. rm-book)
          (mk-resp "Successfully deleted" 200)
          (mk-resp "Deletion is failed" 500))))

    (PUT ["/:id", :id #"[0-9]+"] [:as req]
      (let [params (keywordize-keys (req :params))
            id (-> params :id Integer.)
            filt-params {:author (:author params) :title (:title params)}]
        (if-book-exists id
          (if (update-book filt-params id)
            (mk-resp "Book was upd successfully" 200)
            (mk-resp "Update was failed" 500)))))

    (POST "/:id" [:as req]
      (let [params (keywordize-keys (req :params))
            id (-> params :id Integer.)
            fparams {:author (:author params) :title (:title params)}]
        (if-book-doesnt-exist id
          (if (and (has-value fparams :author) (has-value fparams :title))
            (if (insert-book fparams id)
              (mk-resp "Book was added successfully" 200)
              (mk-resp "Addition  failed" 500))
            (mk-resp "Title and author must be specified for a book" 400)))))

    (GET "/txt" [] (gen-dwn-summary-response "text" books-to-txt))
    (GET "/json" [] (gen-dwn-summary-response "json" books-to-json))
    (GET "/html" [] (gen-dwn-summary-response "html" (wr-page books-to-html)))

    (POST "/" [] (mk-resp (set-books-to-big-read)  200))
    (ANY "/" [] (mk-resp "Not allowed" 403))
  )

  (context "/api" []
     ; accrding to http://www.w3.org/Protocols/rfc2616/rfc2616-sec9.html
     ; body content is not specified (accept info about opts)
    (OPTIONS "/" []
      { :status 200
        :headers {"Allow" "GET, PUT, POST, DETELE, OPTIONS" } 
        :body
        "\n{
         \n  'allow': 'GET, PUT, POST, DETELE, OPTIONS',
         \n  'options':[
         \n    { 'method':'GET', 'path':'/api', 'params':[] },
         \n      { 'method':'GET', 'path':'/api/:id', 'params':[] },
         \n      { 'method':'GET', 'path':'/api/res', 'params':[] },
         \n      { 'method':'GET', 'path':'/api/history', 'params':[] },
         \n      { 'method':'OPTIONS', 'path':'/api', 'params':[] },
         \n      { 'method':'POST', 'path':'/api', 'params':['author'] },
         \n      { 'method':'DELETE', 'path':'/api/:id', 'params':[] },
         \n      { 'method':'PUT', 'path':'/api/:id', 'params':['author'] },
         \n   ]
         \n}
        " 
      } 
    )

    (GET "/" [:as req]
    (if  (= (required-view req) "UNKNOWN") (mk-resp "Unknown output format" 406)
      (with-hist req
        (mk-resp (render-tasks (find-all-tasks) (required-view req)) 200))))
      

    (GET ["/:id", :id #"[0-9]+"] [:as req]
    (if (= (required-view req) "UNKNOWN") (mk-resp "Unknown output format" 406)
      (with-hist req
        (let [params (keywordize-keys (req :params))
              id (-> params :id Integer.)
              task (-> id Integer. find-task)]
          (if (empty? task)
            (mk-resp "Task is unavailable" 404) 
            (mk-resp (render-task task (required-view req)) 200))))))

    (POST "/" [:as req]
    (with-hist req
      (let [params (keywordize-keys (req :params))
            author (:author params)]
        (if (empty? author) (mk-resp "Author is not specified" 400)  
          (let [creation-status (add-task author)]
            (if (first creation-status)
              (mk-resp (str "Task with id " (second creation-status) " was created") 200)
              (mk-resp "Task creation failied" 500)))))))

    (DELETE ["/:id", :id #"[0-9]+"] [:as req id]
    (with-hist req
      (if-task-exists id
        (if (-> id Integer. rm-task)
          (mk-resp "Successfully deleted" 200)
          (mk-resp "Deletion is failed" 500)))))

    (PUT ["/:id", :id #"[0-9]+"] [:as req]
    (with-hist req
      (let [params (keywordize-keys (req :params))
            id (-> params :id Integer.)
            auth (:author params)]
        (if (empty? auth) (mk-resp "Author is not specified" 400)
          (if-task-exists id
            (if (update-task id auth)
              (mk-resp "Task upadate succeed" 200)
              (mk-resp "Update was failed" 500)))))))

    (GET "/stat" [:as req] (with-hist req (mk-resp (get-stat) 200)))
    (GET "/history" [:as req] (with-hist req (mk-resp (get-hist) 200)))

    (ANY "/" [:as req] (with-hist req (mk-resp "Not allowed" 403)))
  )
  (route/resources "/res/")
  (route/not-found "Not Supported"))

(def app
  (->
    (handler/site app-routes)
    (wrap-restful-response)))

;----------------------------------
; TODO:
;
; +post validation
; +options with supported pathes
; +406 on unknown content type
;
; Object Marshaling: [Clojure to Clojure]
;   -authomatic deserialization
