(ns bookpref.data
  (:use [monger.result :only [ok?]])
  (:require [monger.core :as mg]
            [monger.collection :as coll]
            [monger.json]
            [monger.util :as util]
            [monger.joda-time]
            [clj-time.core :as time]))
            
(mg/connect! {:host "localhost", :port 27017})
(mg/set-db! (mg/get-db "books"))

; Book managment

(defn- with-objid [b id] (assoc b :_id id))
(defn clear-books [] (coll/remove "data"))

(defn insert-book [book prior]
  (let [nth-book (with-objid book prior)] (ok? (coll/insert "data" nth-book))))

(defn find-book [prior] (coll/find-maps "data" {:_id prior}))
(defn find-all-books [] (coll/find-maps "data"))
(defn update-book [data id] (ok? (coll/update-by-id "data" id data)))
(defn rm-book [id] (ok? (coll/remove-by-id "data" id)))

; Task managment
; **Stat

(defn add-stat [auth] (coll/insert "stat" {:key auth :_id (util/object-id) }))
(defn get-stat [] (map #(:key %)(coll/find-maps "stat")))

(defn add-req [req] (coll/insert "history" {:req req :_id (util/object-id)
                                            :tm (time/now)}))
(defn get-hist [] 
  (map #(:req %)(sort #(compare (:tm %1) (:tm %2)) (coll/find-maps "history"))))

(defmacro with-stat [auth rest] `(let [tmp# (add-stat ~auth)] ~rest))

(defn- get-task-data [author] 
  (with-stat author (coll/find-maps "data" {:author (re-pattern author)}))
)
(defn- free-task-id []
  (+ (apply max (map #(:_id %) (coll/find-maps "tasks"))) 1))

(defn add-task [auth]
  (let [id (free-task-id)]
    (list (ok? (coll/insert "tasks" {:_id (free-task-id), :author auth, 
                                :content (get-task-data auth)}))
     id)))

(defn find-all-tasks [] (sort #(compare (:_id %1) (:_id %2))
                              (coll/find-maps "tasks")))
(defn find-task [id] (coll/find-one-as-map "tasks" {:_id id}))
(defn rm-task [id] (ok? (coll/remove-by-id "tasks" id)))
(defn update-task [id author]
  (ok? (coll/update-by-id "tasks" id {:author author,
                                     :content (get-task-data author)})))
