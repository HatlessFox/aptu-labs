(ns bookpref.actions
  (:use [clojure.string :only [split join]]
        [local-file]
        bookpref.data)
)

(defn- store-book [book-data]
  (let [id (first book-data), dd (second book-data)]
    (insert-book {:author (second dd), :title (first dd)} id)))

(defn set-books-to-big-read []
  (let [cnt-path (apply str ((juxt project-dir #(str "/resources/" %)) "big_read.txt"))
        cnt (split (slurp cnt-path) #"\n")
        data (map #(split % #" by ") cnt)
        data-to-store (partition 2 (interleave (iterate inc 1) data))
       ]
    (do
     (clear-books)
     (doseq [book data-to-store] (store-book book))
     "Books were set to 'Big Read'"
    )))

(defn book-to-string [book]
  (str (:_id book) ". " (:title book) " written by " (:author book)))

(defn book-to-txt [b] (str (:_id b) ". " (:title b) " written by " (:author b)))
(defn books-to-txt [bs] (join "\n" (map book-to-txt bs)))
(defn json-obj-wrap [data] (str "{ " data " }"))

(defn book-to-json [b] 
  (json-obj-wrap (join ", " [(str "\"id\": \"" (:_id b) "\"")
                            (str "\"title\": \"" (:title b) "\"")
                            (str "\"author\": \"" (:author b) "\"")])))

(defn books-to-json [bs] 
  (str "{\n \"books\":[\n" (join ",\n" (map book-to-json bs)) "\n]}"))

(defn wr-td [d] (str "<td>" d "</td>"))
(defn wr-page [d] (str "<html><body>" d "</body></html>"))
(defn book-to-html [b]
  (str "<tr>" (-> b :_id wr-td) 
              (-> b :author wr-td) 
              (-> b :title wr-td) "</tr>"))
(defn books-to-html [bs]
  (str "<table border=\"1\">" (join "\n" (map book-to-html bs)) "</table>"))

; ==========================
; Tasks

(defn- render [prefix renderer suff task] (str (prefix task) (renderer task) (suff task)))

(def supported-view-types #{"json", "xml", "plain", "html"})
; JSON
(defn- json-attr-name [n] (str "\"" n "\": "))
(defn- json-pref [_] "{\n")
(defn- json-suff [_] "}\n")
(defn- task-to-json [t] 
  (join ",\n" [(str (json-attr-name "id") "\"" (:_id t) "\"")
               (str (json-attr-name "author") "\"" (:author t) "\"")
               (str (json-attr-name "content") 
                    (str "[\n\t" (join ",\n\t" (map book-to-json (:content t))) "\n]\n"))]
))

(defn- render-json [t] ((partial render json-pref task-to-json json-suff) t)) 


; XML
(defn- xml-pref [_] "<task>\n\t")
(defn- xml-suff [_] "\n</task>")
(defn- xml-tag [n v] (str "<" n ">" v "</" n ">"))
(defn- book-to-xml [p b]
  (let [id-tag  (xml-tag "id" (:_id b))
        auth-tag (xml-tag "author" (:author b))
        title-tag (xml-tag "title" (:title b))
        content (join (str "\n\t" p) [(str p "\t" id-tag) auth-tag title-tag])]
    (join "\n" [(str p "<book>") content (str p "</book>")])))

(defn- task-to-xml [t]
  (join "\n\t" [(xml-tag "id" (:_id t)) (xml-tag "author" (:author t))
                (let [books (join "\n" (map (partial book-to-xml "\t\t") (:content t)))
                      content (str "\n" books "\n")]
                  (xml-tag "content" content))
               ]
))

(defn- render-xml [t] ((partial render xml-pref task-to-xml xml-suff) t)) 

; HTML
(defn- html-pref [_] "<html><body><img src=\"/res/pict.png\" height='42' width='42'/> is not on")
(defn- html-suff [_] "</body></html>")
(defn- task-to-html [t]
  (str (xml-tag "h2" (str "Task " (:_id t) " summary"))
       (xml-tag "i" (str "Search by " (:author t)))
        (books-to-html (:content t))))

(defn- render-html [t]  ((partial render html-pref task-to-html html-suff) t))

; TXT
(defn- render-txt [t] 
  (str "Task id " (:_id t) ": search by " (:author t) "\n" 
     (str "\t" (join "\n\t" (map #(str "- " (:title %)) (:content t)))))
)

(defn render-task [task type]
  (cond (= type "json") (render-json task)
        (= type "html") (render-html task)
        (= type "xml")  (render-xml task)
        :else  (render-txt task)
))

; Tasks rendering
;

(defn- render-tasks-common [pr trans sep suff tasks]
  (str pr (join sep (map trans tasks)) suff))

(def render-tasks-json 
  #((partial render-tasks-common "{\n\"content:\"[\n" render-json ",\n" "\n]\n}") %))
(def render-tasks-html
  #((partial render-tasks-common (html-pref "") task-to-html "<br/>" (html-suff "")) %))
(def render-tasks-xml
  #((partial render-tasks-common "<tasks>\n" render-xml "\n" "\n</tasks>") %))
(def render-tasks-plain
  #((partial render-tasks-common "" render-txt "\n" "") %))

(defn render-tasks [tasks type]
  (cond (= type "json") (render-tasks-json tasks)
        (= type "html") (render-tasks-html tasks)
        (= type "xml") (render-tasks-xml tasks)
        :else (render-tasks-plain tasks)))
