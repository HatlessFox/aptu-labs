(defproject bookpref "0.1"
  :description "Books to read"
  :url "TBD"
  :resource-paths ["resources"]
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [compojure "1.1.6"]
                 [ring-middleware-format "0.3.1"]
                 [com.novemberain/monger "1.5.0"]
                 [local-file "0.1.0"] 
                ]
  :plugins [[lein-ring "0.8.8"]]
  :ring {:handler bookpref.handler/app}
  :main ^:skip-aot bookpref.handler
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring-mock "0.1.5"]]}})
