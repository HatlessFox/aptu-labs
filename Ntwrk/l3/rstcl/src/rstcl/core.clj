(ns rstcl.core
  (:require [clj-http.client :as client]
            [clojure.data.json :as json])
)

(defn -main [host port id]
  (let [resp (client/get (str "http://" host ":" port "/api/" id)
                         {:accept :json, :throw-exceptions false})]
    (if (= 200 (:status resp)) 
      (println (json/read-str (:body resp)))
      (println "Status is " (:status resp))))
)
