#include <iostream>
#include <cassert>
#include <strings.h>
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

uint16_t calcChecksum(void * hdr, size_t len) {
  assert(len % sizeof(uint16_t) == 0);

  uint16_t *data = (uint16_t *)hdr;
  uint32_t cs = 0;

  for (size_t i = 0; i < len / sizeof(uint16_t); ++i) { cs += data[i]; }
  while (cs >> 0x10) { cs = (cs >> 0x10) + (cs & 0xFFFF); }

  return ~cs;
}

void sendTimestampRequest(int sock, sockaddr *servAddr, size_t size) {
  icmp pckt;
  bzero(&pckt, sizeof(pckt));

  pckt.icmp_type = ICMP_ECHO;

  pckt.icmp_code = 0;
  pckt.icmp_hun.ih_idseq.icd_id = 0;
  //  pckt.icmp_hun.ih_idseq.icdA_seq = 0;
  //pckt.icmp_dun.id_ts.its_otime = 0;

  pckt.icmp_cksum = calcChecksum(&pckt, sizeof(pckt));
  int sent_bytes;
  if ((sent_bytes = sendto(sock, &pckt, sizeof(pckt), 0, servAddr, size)) < 0) {
    std::cerr << "Unable to send data" << std::endl;
    return;
  };
  std::cout << "Sent bytes" << sent_bytes << std::endl;


  char pack[56+60+76];
  std::cout << recvfrom(sock, &pack, sizeof(pack), 0, servAddr, &size)
 << std::endl;

  icmp *pp =  (icmp *)((char *)pack + 20);

  //if (pp->type == ICMP_ECHOREPLY) {
    std::cout << (int)pp->icmp_type << " " << (int)pp->icmp_code << std::endl;
    //}
}

int main() {
  //create socket
  int icmpSocket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
  if (icmpSocket < 0) {
    std::cerr << "Unable to create socket " <<  std::endl;
    return -1;
  }

  //init connection
  sockaddr_in servAddr;
  bzero(&servAddr, sizeof(servAddr));
  servAddr.sin_family = AF_INET;
  inet_aton("83.149.197.121", &servAddr.sin_addr);
  //  inet_aton("8.8.8.8", &servAddr.sin_addr);

  sendTimestampRequest(icmpSocket, (sockaddr *) &servAddr, sizeof(servAddr));

  close(icmpSocket);
  return 0;
}
